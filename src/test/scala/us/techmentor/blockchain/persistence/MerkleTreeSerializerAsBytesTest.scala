package us.techmentor.blockchain.persistence

import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import us.techmentor.Numbers.intToByteArray
import us.techmentor.blockchain.fact.Fact
import us.techmentor.blockchain.merkle.{MerkleElement, MerkleTree}

import java.util

class MerkleTreeSerializerAsBytesTest
  extends AnyFlatSpec
     with MockFactory
     with should.Matchers {
  "A node" should "have a size an element and children" in {
    val byteArray = Array[Byte](0);
    val merkleTree = createMerkleTree();

    val merkleTreeSerializer = new MerkleTreeSerializer();
    var elementMock = (p: merkleTreeSerializer.SerializeFunctions,m: MerkleElement[Fact])=>byteArray
    var childrenMock= (p: merkleTreeSerializer.SerializeFunctions,m: MerkleElement[Fact])=>"[]".getBytes()

    val p = merkleTreeSerializer.serializeFunctions;
    merkleTreeSerializer.serializeFunctions =
      merkleTreeSerializer.SerializeFunctions(p.node,elementMock,childrenMock,null,null);
    val result = merkleTreeSerializer.asBytes(merkleTree);

    assert(result.sameElements(Array[Byte](0,0,0,1,0,91,93)))
  }
  "An element" should "append factgroup to hash, separated by a colon" in{
    val merkleTreeSerializer = new MerkleTreeSerializer();
    val mockFactGroup: merkleTreeSerializer.SerializeFunction =
      (p,m)=>"123".getBytes();

    val p = merkleTreeSerializer.serializeFunctions;
    val _p= merkleTreeSerializer.SerializeFunctions(
            p.node,
            p.element,
            p.children,mockFactGroup,null);

    val element = createMerkleTree().getRoot
    val result = _p.element(_p,element);
    val expected = (element.hash.get().getValue+":123").getBytes();

    assert(result.sameElements(expected))
  }
  "A fact group" should "return count of fact elements followed by the elements themselves" in {
    val merkleTreeSerializer = new MerkleTreeSerializer();
    val mockFactElement:  java.util.Map.Entry[String,String] => Array[Byte] =
      (_)=>"name=value".getBytes();

    val p = merkleTreeSerializer.serializeFunctions;
    val _p= merkleTreeSerializer.SerializeFunctions(
      p.node,
      p.element,
      p.children,p.factGroup,mockFactElement);

    val element = createMerkleTree().getRoot.left.get()
    val result = _p.factGroup(_p,element)
    val testBytes = "name=value".getBytes()
    val expected = (2.asInstanceOf[Byte] +: testBytes) ++ testBytes;

    assert(result.sameElements(expected));
  }

  "Fact element" should "Turn the map entry into the name=value byte array with sizes" in {
    val merkleTreeSerializer = new MerkleTreeSerializer();

    val entry = new java.util.HashMap[String,String](){{
      this.put("name","value");
    }}.entrySet().toArray()(0).asInstanceOf[java.util.Map.Entry[String,String]];
    val result = merkleTreeSerializer.serializeFunctions.factElement(entry)

    assertResult(Array[Byte](0,0,0,10)++"name=value".getBytes())(result)
  }

  "A children" should "have the count and size attached" in {
    val merkleTreeSerializer = new MerkleTreeSerializer();
    val mockNode: merkleTreeSerializer.SerializeFunction =
      (_,_)=>"hello, world".getBytes()

    val p = merkleTreeSerializer.serializeFunctions;
    val _p= merkleTreeSerializer.SerializeFunctions(
      mockNode,
      p.element,
      p.children,p.factGroup,p.factElement);

    val element = createMerkleTree().getRoot
    val result = _p.children(_p,element)

    val phrase = "hello, world"
    val expected = Array[Byte](2)++intToByteArray(12)++phrase++intToByteArray(12)++phrase


    assertResult(expected)(result);
  }

  "A fact group" should "have only the 0 count if both children exist" in {
    val merkleTreeSerializer = new MerkleTreeSerializer();

    val p = merkleTreeSerializer.serializeFunctions;
    val _p = merkleTreeSerializer.SerializeFunctions(
      p.node,
      p.element,
      p.children, p.factGroup, null);

    val element = createMerkleTree().getRoot
    val result = _p.factGroup(_p, element)

    assert(result.sameElements(Array[Byte](0)));
  }

  def createMerkleTree() =
    new MerkleTree[Fact](){{
      add(new Fact(new util.HashMap[String,String](){{
        this.put("name","inigo montoya")
      }}))
      add(new Fact(new util.HashMap[String,String](){{
        this.put("name","westley")
      }}))
    }}
}
