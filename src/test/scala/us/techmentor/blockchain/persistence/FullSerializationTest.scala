package us.techmentor.blockchain.persistence

import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import us.techmentor.blockchain.fact.Fact
import us.techmentor.blockchain.merkle.MerkleTree

import java.util

class FullSerializationTest
  extends AnyFlatSpec
  with MockFactory
  with should.Matchers {

  "Serialization followed by deserialization" should "result in the same tree being returned" in {
    val tree = {
      new MerkleTree[Fact]() {
        {
          add(new Fact(new util.HashMap[String, String]() {
            {
              this.put("name", "inigo montoya")
            }
          }))
          add(new Fact(new util.HashMap[String, String]() {
            {
              this.put("name", "westley")
            }
          }))
        }
      }
    }

    val merkleTreeSerializer = new MerkleTreeSerializer
    val merkleTreeDeserializer = new MerkleTreeDeserializer

    val b = merkleTreeSerializer.asBytes(tree)
    val result = merkleTreeDeserializer.asTree(b)

    assertEquals(
      tree.getRoot.left.get().value.get().get("name"),
      result.getRoot.left.get().value.get().get("name"))
    assertEquals(
      tree.getRoot.right.get().value.get().get("name"),
      result.getRoot.right.get().value.get().get("name"))
  }
}
