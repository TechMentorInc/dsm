package us.techmentor.learning;

import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.security.KeyPairGenerator;
import java.security.SecureRandom;

public class CryptoLearningTest {

    @Test
    public void passwordBasedEncryptionTest() throws Exception {
        var salt = new byte[]{0,0,0,0,0,0,0,0};
        var IV = new byte[] { 0x01, 0x02, 0x03, 0x04,
            0x05, 0x06, 0x07, 0x08, 0x01, 0x02, 0x03, 0x04,
            0x05, 0x06, 0x07, 0x08  };
        var ps = new javax.crypto.spec.PBEParameterSpec(salt, 20, new IvParameterSpec(IV));
        var secretKey = SecretKeyFactory.getInstance("PBEWithHmacSHA256AndAES_256")
                .generateSecret(new PBEKeySpec("Hello world".toCharArray()));

        var cipher = Cipher.getInstance("PBEWithHmacSHA256AndAES_256");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, ps);

        var clear = ("Four score and seven years ago our " + 
                    "forefathers brought forth upon this " +
                    "continent a new nation");

        byte[] cipherText = cipher.doFinal(clear.getBytes());

        assertNotEquals(clear, new String(cipherText));

        cipher = Cipher.getInstance("PBEWithHmacSHA256AndAES_256");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, ps);
        var resultingClear = new String(cipher.doFinal(cipherText));

        assertEquals(resultingClear, clear);
    }

    @Test
    public void asymmetricKeyTest() throws Exception {
        var clear = ("Four score and seven years ago our " + 
                    "forefathers brought forth upon this " +
                    "continent a new nation");

        var secureRandom = new SecureRandom();
        var keypairGenerator = KeyPairGenerator.getInstance("RSA");

        keypairGenerator.initialize(2048, secureRandom);
        var keypair = keypairGenerator.generateKeyPair();
        var encryptionCipher = Cipher.getInstance("RSA");
        encryptionCipher.init(Cipher.ENCRYPT_MODE,keypair.getPublic());
        
        var cipherText = encryptionCipher.doFinal(clear.getBytes());

        var decryptionCipher = Cipher.getInstance("RSA");
        decryptionCipher.init(Cipher.DECRYPT_MODE,keypair.getPrivate());

        var decryptedText = new String(decryptionCipher.doFinal(cipherText));
        assertEquals(clear,decryptedText);

        // https://www.geeksforgeeks.org/asymmetric-encryption-cryptography-in-java/
    }
}
