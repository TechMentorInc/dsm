package us.techmentor;

import com.google.inject.Injector;
import us.techmentor.dsm.metadata.MetaDataOperations;

public class InjectorUtilities {
    public static void forceInjectorsToUseTempDirectory(Injector[] injectors){
        for(Injector injector : injectors){
            var metaDataOperations = injector.getInstance(MetaDataOperations.class);
            metaDataOperations.forceTempHomeDirectory();
        }
    }
}
