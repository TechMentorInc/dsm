package us.techmentor.dsm.remotes;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;
import us.techmentor.blockchain.connector.TCPClientProtocol;
import us.techmentor.dsm.DSM;
import us.techmentor.dsm.metadata.MetaDataOperations;

import java.util.Arrays;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class RemoteOperationsTest {
    private void forceInjectorsToUseTempDirectory(Injector[] injectors){
        for(Injector injector : injectors){
            var metaDataOperations = injector.getInstance(MetaDataOperations.class);
            metaDataOperations.forceTempHomeDirectory();
        }
    }

    @Test
    public void getAllRemotesTest(){
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[] {injector});

        var remoteOperations = injector.getInstance(RemoteOperations.class);
        remoteOperations.saveRemote("127.0.0.1");
        remoteOperations.saveRemote("192.168.0.1");

        var remotes = remoteOperations.getAllRemotes();

        assertTrue(remotes.contains("127.0.0.1"));
        assertTrue(remotes.contains("192.168.0.1"));
    }

    @Test
    public void createRemoteConnectorsTest(){
        var mockServerOperations = mock(ServerOperations.class);
        var injector = Guice.createInjector();
        var dsm = injector.getInstance(DSM.class);
        dsm.serverOperations.listen();

        var injector2 = Guice.createInjector();
        var remoteOperations = injector2.getInstance(RemoteOperations.class);

        try {
            remoteOperations.createRemoteConnectors(Arrays.asList("127.0.0.1"));
            assertEquals("127.0.0.1",
            ((TCPClientProtocol) remoteOperations
                    .getAllConnectors()
                    .get(0).getProtocol()
            ).getIpAddress());

        } finally {
            remoteOperations.getAllConnectors().get(0).stopPolling();
            dsm.stop();
        }
    }
}
