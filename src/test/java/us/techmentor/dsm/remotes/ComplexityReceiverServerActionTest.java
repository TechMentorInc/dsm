package us.techmentor.dsm.remotes;

import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import scala.Array;
import us.techmentor.Hash;
import us.techmentor.blockchain.connector.ServerProtocol;
import us.techmentor.blockchain.speed.complexity.Action;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class ComplexityReceiverServerActionTest {
    @Test
    public void serveClientTest() throws IOException {
        var serverOperationsMock = mock(ServerOperations.class);
        var protocolMock = mock(ServerProtocol.class);

        var inputStreamMock = mock(InputStream.class);
        when(serverOperationsMock.getServerProtocolForClient(new NetworkClientAcceptor.NetworkClient("169.254.1.2",inputStreamMock,null,null,null))).thenReturn(protocolMock);
        var networkClientMock = new NetworkClientAcceptor.NetworkClient("169.254.1.2",inputStreamMock,null,null,null);

        var testString = ("1234,Propose,"+DateTime.now().plusDays(1).getMillis()+",FFFF").getBytes();
        when(inputStreamMock.available()).thenReturn(testString.length+1);
        doAnswer(invocation->{
            var arg = invocation.getArgument(0,byte[].class);
            Array.copy(testString,0,arg,0,testString.length);
            return testString.length;
        }).when(inputStreamMock).read(any());

        var suggestComplexityServerAction = new ComplexityReceiverServerAction();
        suggestComplexityServerAction.serverOperations = serverOperationsMock;
        suggestComplexityServerAction.serveClient(networkClientMock);

        var complexityProposalCaptor = ArgumentCaptor.forClass(ComplexityProposal.class);
        verify(protocolMock).addComplexitySuggestionToIncomingQueue(complexityProposalCaptor.capture());

        var complexityProposal = complexityProposalCaptor.getValue();
        assertEquals(Hash.fromString("1234"),complexityProposal.id());
        assertEquals(Action.PROPOSE,complexityProposal.action());
        assertTrue(complexityProposal.time().isAfter(DateTime.now()));
        assertEquals(
            new ComplexityDefinition(BigInteger.valueOf(0xffff)),
            complexityProposal.complexityDefinition());
    }
}
