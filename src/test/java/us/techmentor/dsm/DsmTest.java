package us.techmentor.dsm;

import com.google.inject.Injector;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.mockito.ArgumentCaptor;
import us.techmentor.blockchain.*;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;
import us.techmentor.blockchain.fact.FactQuery.StringTruth;
import us.techmentor.blockchain.fact.FactQuery.Truth;
import us.techmentor.dsm.person.Actor;
import us.techmentor.dsm.person.ActorCrypto;
import us.techmentor.dsm.metadata.MetaDataOperations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static us.techmentor.InjectorUtilities.forceInjectorsToUseTempDirectory;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import us.techmentor.dsm.remotes.*;

/*
https://www.baeldung.com/docker-java-api
https://github.com/tcurdt/jdeb/blob/master/docs/maven.md
*/
public class DsmTest {

    @Test
    public void createActor() throws InterruptedException {
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        var dsm = injector.getInstance(DSM.class);
        var factOperations = injector.getInstance(FactOperations.class);

        var finished = new AtomicBoolean(false);
        dsm.createActor("Fred", "flintstone",a->finished.set(true));

        while(!finished.get());

        var factQuery = new FactQuery(Map.<String,Truth>of("type", new StringTruth("actor-creation")));
        var fact = factOperations.readFact(factQuery).get();

        assertEquals("Fred",fact.get("name"));
    }

    @Test
    public void becomeTest() throws InterruptedException{
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        var dsm = injector.getInstance(DSM.class);
        try{
            var result = new HashMap<String,Object>();

            var finished = new AtomicBoolean(false);
            dsm.createActor("Fred", "fredspassword",(actor)->{
                try{
                    assertNull(dsm.getCurrentActor());

                    dsm.become("Fred","fredspassword");

                    var current= dsm.getCurrentActor();
                    assertEquals(actor.getHash().getValue(),current.getHash().getValue());

                    result.put("functionExecuted",true);
                    finished.set(true);
                }catch(Throwable t){
                    t.printStackTrace();
                }
            });

            while(!finished.get());
            assertTrue((boolean)result.get("functionExecuted"));
        }finally{dsm.stop();}
    }

    @Test
    public void becomeNonExistentUserFailsTest() throws InterruptedException{
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        var dsm = injector.getInstance(DSM.class);

        try{
            var functionExecuted = new AtomicBoolean(false);
            dsm.createActor("Fred", "fredspassword",(actor)->{
                assertNull(dsm.getCurrentActor());

                try{
                    dsm.become("Wilma","fredspassword");
                }catch (Throwable t){}
                assertNull(dsm.getCurrentActor());
                functionExecuted.set(true);
            });

            for(int i=0;i<40&&!functionExecuted.get();i++){
                Thread.sleep(100);
            }
            assertTrue(functionExecuted.get());
        }finally{dsm.stop();}
    }

    @Test
    public void postTest() throws Exception{
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        Map<String,Object> results = new HashMap<String,Object>();

        var dsm = injector.getInstance(DSM.class);

        try{
        var factOperations = injector.getInstance(FactOperations.class);
        dsm.createActor("Fred", "fredspassword",(a)->{
            results.put("actor",a);
        });
        try {
            for(int i=0; i<100; i++){
                if(results.get("actor")!=null) break;
                Thread.sleep(100);
            }
            dsm.become("Fred","fredspassword");
            dsm.post("This is some interesting content");

            Optional<Fact> fact = Optional.empty();
            for(int i=0; i<100; i++){
                fact = factOperations.readFact(
                    new FactQuery(
                        Map.of(
                            "type", new StringTruth("post")
                        )
                    )
                );
                if(fact.isPresent()) break;
                Thread.sleep(100);
            }

            var actor = (Actor)results.get("actor");
            String clearText =
            ActorCrypto
                .getInstance()
                .publicKeyDecryptFromBase64(
                    actor.getActorKey(), fact.get().get("content")
                );
            assertTrue(clearText.contains("some interesting"));
        } catch (Exception e) {e.printStackTrace();throw e;}
        }finally{ dsm.stop(); }
    }

    @Test
    public void followTest() throws InterruptedException{
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        Map<String,Object> results = new HashMap<String,Object>();

        var dsm = injector.getInstance(DSM.class);

        try{
        for (int i=1; i<3; i++){
            final int j = i;
            dsm.createActor("actor"+j, "password"+i,(a)->{
                results.put("actor"+j,a);
            });
        }

        for(int i=0; i<100; i++){
            boolean result = true;
            for(int j=1; j<3; j++){
                result = results.get("actor"+j)!=null&&result;
            }
            if(result) break;
            Thread.sleep(100);
        }

        var actor2 = ((Actor)results.get("actor2"));
        dsm.become("actor1","password1");
        dsm.follow(actor2);

        assertEquals("actor1",dsm.getCurrentActor().getName());
        List<Actor> following =
            dsm.getCurrentActor().whoAmIFollowing();
        assertTrue(
            following
            .stream()
            .map(
                a->a.getName())
                .collect(
                    Collectors.toList())
            .contains("actor2"));
        }finally{dsm.stop();}
    }

    @Test
    public void subscriberCanReadPostTest() throws InterruptedException{
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        var dsm = injector.getInstance(DSM.class);

        try{
        Map<String,Object> results = new HashMap<String,Object>();

        for (int i=1; i<3; i++){
            final int j = i;
            dsm.createActor("actor"+j, "password"+i,(a)->{
                results.put("actor"+j,a);
            });
        }
        for(int i=0; i<100; i++){
            boolean result = true;
            for(int j=1; j<3; j++){
                result = results.get("actor"+j)!=null&&result;
            }
            if(result) break;
            Thread.sleep(100);
        }
        var actor2 = (Actor)results.get("actor2");
        dsm.become("actor1","password1");
        dsm.follow(actor2);

        dsm.become("actor2","password2");
        dsm.post("This is some sick content!");

        dsm.become("actor1","password1");

        var contents = dsm.getMostRecentContent(5);

        var contentList = contents
            .stream()
            .map(c->c.getContentText())
            .collect(Collectors.toList());
        assertTrue(contentList.contains("This is some sick content!"));
        }finally{dsm.stop();}
    }
    @Test
    public void addRemoteTest(){
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        var dsm = injector.getInstance(DSM.class);

        try{
            var result = dsm.addRemoteByIPAddress("127.0.0.1");
            var remoteOperations = injector.getInstance(RemoteOperations.class);
            var remotes = remoteOperations.getAllRemotes();
            assertEquals("Added remote [127.0.0.1]", result);
            assertTrue(remotes.contains("127.0.0.1"));
        }finally{dsm.stop();}
    }

    @Test
    public void clientToServerCreateActor() throws InterruptedException {
        var mockFactOperations = mock(FactOperations.class);

        var injector1 = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                super.configure();
                bind(FactOperations.class).toInstance(mockFactOperations);
            }
        });
        var injector2 = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector1,injector2});

        var dsm = injector1.getInstance(DSM.class);
        dsm.serverOperations.setFactOperations(mockFactOperations);
        dsm.serverOperations.listen();

        var dsm2 = injector2.getInstance(DSM.class);

        try {
            assertNotSame(dsm, dsm2);
            dsm2.addRemoteByIPAddress("127.0.0.1");
            dsm2.heartbeat.remoteOperations.createRemoteConnectors(dsm2.heartbeat.remoteOperations.getAllRemotes());

            var done = new AtomicBoolean(false);
            dsm2.createActor("Kyle","Test",(a)->{
                done.set(true);
            });

            while(!done.get()){Thread.sleep(100);}
            
            var factCaptor = ArgumentCaptor.forClass(Fact.class);
            verify(mockFactOperations,timeout(10000)).saveFact(factCaptor.capture());

            assertEquals("Kyle", factCaptor.getValue().get("name"));
        }finally {
            dsm.stop();
            dsm2.stop();
        }
    }
    @Test
    public void clientToServerFactTest() throws InterruptedException {
        var mockFactOperations = mock(FactOperations.class);

        var injector1 = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                super.configure();
                bind(FactOperations.class).toInstance(mockFactOperations);
            }
        });
        var injector2 = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector1,injector2});

        var dsm = injector1.getInstance(DSM.class);

        dsm.serverOperations.listen();
        var dsm2 = injector2.getInstance(DSM.class);

        try {
            assertNotSame(dsm,dsm2);
            dsm2.addRemoteByIPAddress("127.0.0.1");
            dsm2.heartbeat.remoteOperations.createRemoteConnectors(dsm2.heartbeat.remoteOperations.getAllRemotes());

            var fact = Fact.of("id", "1234", "message", "hello world");
            dsm2.remoteOperations.getAllConnectors().get(0).sendFact(fact);

            var factCaptor = ArgumentCaptor.forClass(Fact.class);
            verify(mockFactOperations,timeout(2000)).saveFact(factCaptor.capture());

            assertEquals("hello world", factCaptor.getValue().get("message"));
        }finally {
            dsm.stop();
            dsm2.stop();
        }
    }

    @Test
    public void serverToClientFactTest() {
        var mockFactOperations = mock(FactOperations.class);

        var injector1 = Guice.createInjector();
        var injector2 = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                super.configure();
                bind(FactOperations.class).toInstance(mockFactOperations);
            }
        });
        forceInjectorsToUseTempDirectory(new Injector[]{injector1,injector2});

        var server = injector1.getInstance(DSM.class);
        server.serverOperations.listen();
        var client = injector2.getInstance(DSM.class);

        try {
            client.remoteOperations.setFactOperations(mockFactOperations);
            client.addRemoteByIPAddress("127.0.0.1");
            client.heartbeat.remoteOperations.createRemoteConnectors(client.heartbeat.remoteOperations.getAllRemotes());

            Thread.sleep(4000);

            var connectorFromServer =
            server
                .serverOperations
                .getServerConnectorForClient(new NetworkClientAcceptor.NetworkClient("127.0.0.1",null,null,null,null));
            Thread.sleep(4000);

            var fact = Fact.of("id", "1234", "message", "hello world");
            connectorFromServer.sendFact(fact);

            var factCaptor = ArgumentCaptor.forClass(Fact.class);
            verify(mockFactOperations,timeout(6000)).saveFact(factCaptor.capture());

            assertEquals("hello world", factCaptor.getValue().get("message"));
        } catch(InterruptedException ie){

        } finally {
            client.stop();
            server.stop();
        }
    }

    @Test
    public void serverToClientChainTest() throws InterruptedException {
        var mockChainOperations = mock(ChainOperations.class);
        var injectorClient = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                super.configure();
                bind(ChainOperations.class).toInstance(mockChainOperations);
            }
        });
        var injectorServer = Guice.createInjector();
        var injector3 = Guice.createInjector();

        forceInjectorsToUseTempDirectory(new Injector[]{injectorClient,injectorServer,injector3});

        var b3FactOperations = injector3.getInstance(FactOperations.class);
        var finished = new AtomicBoolean(false);
        var b3 = injector3.getInstance(Blockchain.class);
        b3FactOperations.saveFact(new Fact(Map.of("id","123","message","hello world")),b->finished.set(true));

        while(!finished.get());
        finished.set(false);

        b3FactOperations.saveFact(new Fact(Map.of("id","456")),b->finished.set(true));
        while(!finished.get());

        var server = injectorServer.getInstance(DSM.class);
        server.serverOperations.listen();
        var client = injectorClient.getInstance(DSM.class);

        try {
            client.addRemoteByIPAddress("127.0.0.1");
            client.heartbeat.remoteOperations.createRemoteConnectors(client.heartbeat.remoteOperations.getAllRemotes());

            var connectorFromServer =
                server
                    .serverOperations
                    .getServerConnectorForClient(new NetworkClientAcceptor.NetworkClient("127.0.0.1",null,null,null,null));

            connectorFromServer.updateChain(b3.getChain());

            var factCaptor = ArgumentCaptor.forClass(Chain.class);
            verify(mockChainOperations, timeout(8000)).updateChain(factCaptor.capture());
            var testQuery = Map.<String, FactQuery.Truth>of("id",new FactQuery.StringTruth("123"));

            assertEquals("hello world",
                factCaptor
                .getValue()
                .locateFacts(new FactQuery(testQuery))
                .get(0)
                .get("message"));

        } finally {
            client.stop();
            server.stop();
        }
    }

    @Test
    public void saveTerminateAndThenLoadFromDiskBlockchain() throws InterruptedException {
        var injector1 = Guice.createInjector();
        var dsm = injector1.getInstance(DSM.class);
        var injector2 = Guice.createInjector();
        var dsm2 = injector2.getInstance(DSM.class);

        try{
            var dsmInformation = injector1.getInstance(MetaDataOperations.class);
            dsmInformation.forceTempHomeDirectory();
            var home = dsmInformation.getMetaData().rootHomeDirectory();
            dsm.regularlyLoadAndSave();

            Thread.sleep(1000);

            var finished = new AtomicInteger(0);
            dsm.createActor("Kyle","nunyabizness",a->finished.addAndGet(1));
            while(finished.get()!=1);

            Thread.sleep(1000);
            System.out.println("Home Directory: "+home.toString());


            var dsmInformation2 = injector2.getInstance(MetaDataOperations.class);
            dsmInformation2.manualSetHomeDirectory(home);
            dsm2.regularlyLoadAndSave();
            Thread.sleep(2000);

            assertEquals(1,dsm2.chainOperations.getChain().getBlocks().size());
        }finally{ dsm.stop(); dsm2.stop(); }
    }
    @Test
    public void incomingPollWithoutRegistrationTest(){
        var mockFactOperations = mock(FactOperations.class);

        var injector2 = Guice.createInjector();
        var injector1 = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                super.configure();
                bind(FactOperations.class).toInstance(mockFactOperations);
            }
        });
        forceInjectorsToUseTempDirectory(new Injector[]{injector1,injector2});

        var server = injector1.getInstance(DSM.class);
        server.serverOperations.listen();
        var client = injector2.getInstance(DSM.class);

        try {
            client.remoteOperations.setFactOperations(mockFactOperations);
            client.addRemoteByIPAddress("127.0.0.1");
            client.heartbeat.remoteOperations.createRemoteConnectorsWithoutRegistration(
                    client.heartbeat.remoteOperations.getAllRemotes());

            var fact = Fact.of("id", "1234", "message", "hello world");
            client.remoteOperations.getAllConnectors().get(0).sendFact(fact);

            verify(mockFactOperations,times(0)).saveFact(any());
        }catch(Throwable t){
            t.printStackTrace();
            Assert.fail();
        } finally {
            client.stop();
            server.stop();
        }
    }
    @Test
    public void incomingPollWithoutRegistrationShouldntRespondToCommandsTest(){

        var spyServerOperations = spy(ServerOperations.class);

        var injector2 = Guice.createInjector();
        var injector1 = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                super.configure();
                bind(ServerOperations.class).toInstance(spyServerOperations);
            }
        });
        forceInjectorsToUseTempDirectory(new Injector[]{injector1,injector2});

        var server = injector1.getInstance(DSM.class);
        server.serverOperations.listen();
        var client = injector2.getInstance(DSM.class);

        try {
            client.addRemoteByIPAddress("127.0.0.1");
            client.heartbeat.remoteOperations.createRemoteConnectorsWithoutRegistration(
                    client.heartbeat.remoteOperations.getAllRemotes());

            var fact = Fact.of("id", "1234", "message", "hello world");
            client.remoteOperations.getAllConnectors().get(0).sendFact(fact);

            verify(spyServerOperations,times(0)).processCommand(any(),any());
        }catch(Throwable t){
            t.printStackTrace();
            Assert.fail();
        } finally {
            client.stop();
            server.stop();
        }
    }
}
