package us.techmentor.dsm.connect;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import us.techmentor.dsm.Remote;

public class SourceCoordinationTest {
    @Test
    public void testAddRemote(){
        var remote = new Remote();
        var sourceCoordination = new NoLocalSourceCoordination();
        sourceCoordination.addRemoteSource(remote);

        assertTrue(
            sourceCoordination
            .getSourcesAsList()
            .contains(new RemoteSource(remote)));
    }
    
    @Test
    public void testSubscribe(){
        var sourceCoordination = new EasyLocalSourceCoordination();

        Subscription subscription = 
            ((info)->{});

        sourceCoordination.subscribe(subscription);
        var source = sourceCoordination.local;
        
        assertTrue(source.subscriptions.contains(subscription));
    }

    @Test
    public void testSend(){
        var sourceCoordination = new EasyLocalSourceCoordination();
        sourceCoordination.send("something");
        assertEquals("something",sourceCoordination.local.data);
    }

    @Test
    public void testReceive() throws InterruptedException{

        var stringBuffer = new StringBuffer();
        var sourceCoordination = new EasyLocalSourceCoordination();
        sourceCoordination.subscribe((s)->{
            stringBuffer.append(s.data);
        });
        Thread.sleep(200);
        sourceCoordination.send("test");
    
        Thread.sleep(200);
        assertEquals("test",stringBuffer.toString());
    }
}
class EasySource extends Source {
    String data;
    public EasySource(){}

    @Override
    public void send(Object data) {
        super.send(data);
        this.data = (String)data;
    }
}
class EasyLocalSourceCoordination extends SourceCoordination{
    EasySource local = new EasySource();
    @Override
    protected Source getLocalSource() {
        return local;
    }
}
class NoLocalSourceCoordination extends SourceCoordination{
    @Override
    protected Source getLocalSource() {
        return null;
    }
};