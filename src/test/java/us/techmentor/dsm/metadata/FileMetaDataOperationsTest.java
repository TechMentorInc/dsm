package us.techmentor.dsm.metadata;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static us.techmentor.InjectorUtilities.forceInjectorsToUseTempDirectory;


public class FileMetaDataOperationsTest {
    @Test
    public void testGetId_LoadFromDisk_NoExist(){
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        var fileMetatDataOperations =
            injector.getInstance(FileMetaDataOperations.class);

        var id = fileMetatDataOperations.getID();
        assertTrue(id.length()>8);
    }

    @Test
    public void testGetId_LoadFromDisk_NotExist_And_Exist(){
        var injector = Guice.createInjector();
        forceInjectorsToUseTempDirectory(new Injector[]{injector});

        var fileMetatDataOperations =
                injector.getInstance(FileMetaDataOperations.class);

        var id = fileMetatDataOperations.getID();

        assertTrue(id.length()>8);

        assertEquals(fileMetatDataOperations.getID(), id);
    }

    @Test
    public void testGetId_LoadFromDisk_Mocked(){
        var mockIdOperations = mock(IdOperations.class);
        when(mockIdOperations.loadFromDisk(any())).thenReturn(Optional.of("blah"));

        var fileMetaDataOperations = new FileMetaDataOperations();
        fileMetaDataOperations.idOperations = mockIdOperations;

        var resultId = fileMetaDataOperations.getID();
        assertEquals("blah", resultId);
    }
}
