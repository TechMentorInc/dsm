package us.techmentor.dsm.person;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ActorCryptoTest {
    @Test
    public void testPrivateKeyEncryptBase64_DecryptPublicKeyFromBase64() throws Exception{
        var actorKey = ActorCrypto.getInstance().createActorKey("password");

        var actorCrypto = new ActorCrypto();
        var encrypted = actorCrypto.privateKeyEncryptBase64(actorKey,"test string");
        var decrypted = actorCrypto.publicKeyDecryptFromBase64(actorKey, encrypted);
        
        assertEquals("test string", decrypted);        
    }
}
