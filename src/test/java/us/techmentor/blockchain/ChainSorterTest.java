package us.techmentor.blockchain;

import org.junit.Test;
import us.techmentor.Hash;

import java.util.ArrayList;
import java.util.Vector;

import static org.junit.Assert.assertSame;

public class ChainSorterTest {

    @Test
    public void sortTest(){
        var hash1 = Hash.create("hash1");
        var hash2 = Hash.create("hash2");
        var block1 = new Block(){{
            this.previousBlockHash=null;
            this.hash = hash1;
        }};
        var block2 = new Block(){{
            this.previousBlockHash=hash1;
            this.hash=hash2;
        }};
        var chain = new Chain();

        chain.add(block2);
        chain.add(block1);
        chain.sort();

        assertSame(chain.getBlocks().get(0),block1);
    }
}
