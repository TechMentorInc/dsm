package us.techmentor.blockchain;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import us.techmentor.blockchain.connector.BlockchainConnector;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;
import us.techmentor.blockchain.fact.FactQuery.StringTruth;
import us.techmentor.blockchain.fact.FactQuery.Truth;

@RunWith(MockitoJUnitRunner.class)
public class BlockchainTest {

    static final String SERVER = "server";
    static final String CLIENT = "client";

    @Test
    public void createFactAndReadFactTest() throws Throwable{
        var injector = Guice.createInjector();
        var factOperations = injector.getInstance(FactOperations.class);

        var testSubstance = Map.of("id","1234","message","hello world");
        factOperations.saveFact(new Fact(testSubstance));
        Thread.sleep(1000);

        var testQuery = Map.<String,Truth>of("id",new StringTruth("1234"));
        var resultFact = factOperations.readFact(new FactQuery(testQuery));

        assertEquals(resultFact.get().get("message").toString(), "hello world");
    }

    @Test
    public void createTwoFactsAndReadAppropriateOneBackTest() throws Throwable{
        var injector = Guice.createInjector();
        var factOperations = injector.getInstance(FactOperations.class);

        var finished = new AtomicInteger(0);
        factOperations.saveFact(
            new Fact(Map.of("id","1234","message","hello world")),b->finished.addAndGet(b.facts.asList().size()));
        factOperations.saveFact(
            new Fact(Map.of("id","5678","message","goodbye world")),b->finished.addAndGet(b.facts.asList().size()));

        while(finished.get()!=2);

        var resultFact = factOperations.readFact(new FactQuery(Map.<String,Truth>of("id",new StringTruth("1234"))));
        var resultFact2 = factOperations.readFact(new FactQuery(Map.<String,Truth>of("id",new StringTruth("5678"))));

        assertEquals(resultFact.get().get("message").toString(), "hello world");
        assertEquals(resultFact2.get().get("message").toString(), "goodbye world");
    }

    @Test
    public void saveFactMultinodeAddToQueueTest() {
        BlockchainConnector blockchainConnectorMock = mock(BlockchainConnector.class);
        var blockPreparationMock = mock(BlockPreparation.class);

        Fact fact = new Fact(Map.of("id", "1234"));

        var blockchain = new Blockchain();
        var factOperations = new FactOperations();
        factOperations.blockchain = blockchain;

        blockchain.blockchainConnectors = Set.of(blockchainConnectorMock);
        factOperations.blockPreparation = blockPreparationMock;
        factOperations.saveFact(fact);

        verify(blockPreparationMock).addFactToQueue(eq(fact),any(),any());
    }

    @Test
    public void saveFactMultinodeSendToConnectorsTest() {
        BlockchainConnector blockchainConnectorMock = mock(BlockchainConnector.class);
        var blockPreparationMock = mock(BlockPreparation.class);

        Fact fact = new Fact(Map.of("id", "1234"));

        var blockchain = new Blockchain();
        var factOperations = new FactOperations();
        factOperations.blockchain = blockchain;
        blockchain.blockchainConnectors = Set.of(blockchainConnectorMock);
        factOperations.blockPreparation = blockPreparationMock;
        factOperations.saveFact(fact);

        verify(blockchainConnectorMock).sendFact(fact);
    }


    @Test
    public void checkThatCloneWorks() throws InterruptedException{
        Injector injector1 = Guice.createInjector();
        Injector injector2 = Guice.createInjector();

        var b1 = injector1.getInstance(Blockchain.class);
        var b1FactOperations = injector1.getInstance(FactOperations.class);
        var b2 = injector2.getInstance(Blockchain.class);

        assertNotSame(b1, b2);

        var finished = new AtomicInteger(0);
        b1FactOperations.saveFact(new Fact(Map.of("id","123")),b->finished.addAndGet(1));
        b1FactOperations.saveFact(new Fact(Map.of("id","456")),b->finished.addAndGet(1));

        while(finished.get()>2);

        b2.chain = (Chain)b1.chain.clone();
        assertEquals(b1.chain.getChainLength(), b2.chain.getChainLength());
    }
}
