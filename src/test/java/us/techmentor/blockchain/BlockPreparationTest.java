package us.techmentor.blockchain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import us.techmentor.Hash;
import us.techmentor.blockchain.fact.Fact;

import static us.techmentor.blockchain.BlockPreparation.*;

public class BlockPreparationTest {
    @Test
    public void createBlockTest() throws InterruptedException {
        var blockPreparation = new BlockPreparation();
        var chain = new Chain();
        var fact = new Fact(Map.of("id","1234"));

        blockPreparation.addFactToQueue(
            fact, chain, 
            (b)->{
                chain.add(b);
            }
        );
        
        for(int i=0; i<100; i++){
            if(chain.getChainLength()>0) break;
            Thread.sleep(100);
        }
        
        assertEquals(chain.getAllFacts().get(0),fact);
    }

    @Test
    public void findHashTest(){
        var block = new Block();
        block.getFacts().add(new Fact(Map.of("id","1234")));
        block.setPreviousBlockHash(Hash.create("hello world"));

        var blockPreparation = new BlockPreparation();
        var createdHash = blockPreparation.findHash(block);
    
        assertTrue(
            createdHash
            .asBigInteger()
            .compareTo(blockPreparation.target)<0
        );
    }

    @Test
    public void createValidChainWithTwoFactsTest() throws InterruptedException {
        
        var blockPreparation = new BlockPreparation();
        var chain = new Chain();
        var fact = new Fact(Map.of("id","1234"));

        var finished = new AtomicInteger(0);
        blockPreparation.addFactToQueue(
            fact, chain, (b)->{chain.add(b);finished.addAndGet(b.facts.asList().size());}
        );
        while(finished.get()!=1);

        blockPreparation.addFactToQueue(
            fact, chain, (b)->{chain.add(b);finished.addAndGet(b.facts.asList().size());}
        );
        while(finished.get()!=2);

        var blocks = chain.getBlocks();
        var block1 = blocks.get(0);
        var block2 = blocks.get(1);
        assertEquals(block2.previousBlockHash,block1.hash);
        assertEquals(Hash.create(block2),block2.hash);
        assertNull(block1.previousBlockHash);
        assertEquals(Hash.create(block1),block1.hash);
    }

    @Test
    public void addFactToQueueFactOnQueueTest(){
        var blockPreparation = new BlockPreparation();
        Fact fact = new Fact(Map.of("id","1234"));
        
        blockPreparation.addFactToQueue(fact,null,(b)->{});
        assertEquals(fact,blockPreparation.queue.peek());
    }
}
