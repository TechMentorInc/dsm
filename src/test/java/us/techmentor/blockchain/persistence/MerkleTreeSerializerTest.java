package us.techmentor.blockchain.persistence;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.Map;
import org.junit.Test;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.merkle.MerkleTree;

public class MerkleTreeSerializerTest {
    @Test
    public void merkleTreeAsDataTest(){
        var fact = new Fact[3];
        var merkleTree = new MerkleTree<Fact>();

        for(int i=0;i<3;i++){
            fact[i]= new Fact(Map.of("fact",""+i));
            merkleTree.add(fact[i]);
        }

        var merkleTreeSerializer = new MerkleTreeSerializer();
        var bytes = merkleTreeSerializer.asBytes(merkleTree);
        var f0hash = fact[0].getHash().getValue().getBytes();

        assertTrue(new String(bytes).contains(new String(f0hash)));
    }

    @Test
    public void dataAsMerkleTreeTest(){
        // production code
        // 1) take in byte array
        // 2) turn into tree
    }
}
