package us.techmentor.blockchain;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import org.junit.Test;

import us.techmentor.blockchain.fact.Fact;

import static org.junit.Assert.*;

public class ChainComparatorTest {
    @Test
    public void compareSizeTest() throws InterruptedException {
        Injector injector1 = Guice.createInjector();
        Injector injector2 = Guice.createInjector();

        var b1 = injector1.getInstance(Blockchain.class);
        var b1FactOperations = injector1.getInstance(FactOperations.class);
        var b2 = injector2.getInstance(Blockchain.class);

        assertNotSame(b1, b2);

        var finished = new AtomicInteger(0);
        b1FactOperations.saveFact(new Fact(Map.of("id","123")), b->finished.addAndGet(b.facts.asList().size()));
        b1FactOperations.saveFact(new Fact(Map.of("id","456")), b->finished.addAndGet(b.facts.asList().size()));
        while(finished.get()!=2);

        b2.chain = (Chain)b1.chain.clone();
        b1FactOperations.saveFact(new Fact(Map.of("id","789")), b->finished.addAndGet(b.facts.asList().size()));
        while(finished.get()!=3);

        var comparator = new ChainComparator();
        var result = comparator.compare(b1.chain,b2.chain);
        
        assertEquals(result.longer, b1.chain);
        assertEquals(result.shorter, b2.chain);    
    }

    @Test
    public void compareCommonIndexTest() throws InterruptedException{
        Injector injector1 = Guice.createInjector();
        Injector injector2 = Guice.createInjector();

        var b1 = injector1.getInstance(Blockchain.class);
        var b1FactOperations = injector1.getInstance(FactOperations.class);
        var b2 = injector2.getInstance(Blockchain.class);

        assertNotSame(b1, b2);

        var fact1 = new Fact(Map.of("id","123"));
        var fact2 = new Fact(Map.of("id","456"));
        b1FactOperations.saveFact(fact1);
        b1FactOperations.blockUntilFactOnChain(fact1);
        b1FactOperations.saveFact(fact2);
        b1FactOperations.blockUntilFactOnChain(fact2);

        b2.chain = (Chain)b1.chain.clone();
        var fact3 = new Fact(Map.of("id","789"));
        b1FactOperations.saveFact(fact3);
        b1FactOperations.blockUntilFactOnChain(fact3);

        var comparator = new ChainComparator();
        var result = comparator.compare(b1.chain,b2.chain);

        assertEquals(1,result.lastCommonIndex);
    }

    @Test
    public void compareValidityTest() throws InterruptedException{
        Injector injector1 = Guice.createInjector();
        Injector injector2 = Guice.createInjector();

        var b1 = injector1.getInstance(Blockchain.class);
        var b1FactOperations = injector1.getInstance(FactOperations.class);

        var b2 = injector2.getInstance(Blockchain.class);

        assertNotSame(b1, b2);

        var fact1 = new Fact(Map.of("id","123"));
        var fact2 = new Fact(Map.of("id","456"));
        b1FactOperations.saveFact(fact1);
        b1FactOperations.blockUntilFactOnChain(fact1);
        b1FactOperations.saveFact(fact2);
        b1FactOperations.blockUntilFactOnChain(fact2);

        var fact3=new Fact(Map.of("id","789"));
        b2.chain = (Chain)b1.chain.clone();
        b1FactOperations.saveFact(fact3);
        b1FactOperations.blockUntilFactOnChain(fact3);

        var comparator = new ChainComparator();
        var result = comparator.compare(b1.chain,b2.chain);

        assertEquals(b1.chain.getChainLength(),b2.chain.getChainLength()+1);
        assertTrue(result.bothValid);
        assertTrue(result.lastCommonIndex>0);
    }
}
