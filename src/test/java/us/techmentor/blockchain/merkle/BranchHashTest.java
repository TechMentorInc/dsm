package us.techmentor.blockchain.merkle;

import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.Optional;

import org.junit.Test;

import us.techmentor.Hash;
import us.techmentor.blockchain.fact.Fact;

public class BranchHashTest {

    @Test
    public void constructFromLeafTest(){

        var merkleTree = new MerkleTree<Fact>();
        merkleTree.root.right = Optional.of(
            new MerkleElement<Fact>(){{
                this.hash = Optional.of(Hash.create("blah"));
                this.parent = Optional.of(merkleTree.root);
            }}
        );
        var leaf = new MerkleElement<Fact>(){{
            this.value = Optional.of(new Fact(Map.of("id","1234")));
        }};
        merkleTree.root.left = Optional.of(
            new MerkleElement<Fact>(){{
                this.right = Optional.of(
                    new MerkleElement<Fact>(){{
                        this.hash = Optional.of(Hash.create("foo"));
                    }}
                );
                this.left = Optional.of(leaf);
                this.parent = Optional.of(merkleTree.root);
            }}
        );
        leaf.parent = merkleTree.root.left;
        BranchHash.constructFromLeaf(leaf);

        var leafHash = Hash.getOrCreate(leaf.value.get());
        var leftRightHash = merkleTree.root.left.get().right.get().hash;
        var leftHash = 
            Hash.create(
                "\""+leafHash.getValue()+"\":\""+
                     leftRightHash.get().getValue()+"\"");

        var rightHash = merkleTree.root.right.get().hash;

        var totalHash = 
            Hash.create(
                "\""+leftHash.getValue()+"\":\""+
                     rightHash.get().getValue()+"\"");

        assertEquals(totalHash.getValue(),merkleTree.root.hash.get().getValue());
    }
}
