package us.techmentor.blockchain.speed;

import org.checkerframework.dataflow.qual.TerminatesExecution;
import org.junit.Test;

import java.security.SecureRandom;

import static org.junit.Assert.*;

public class StatisticsTest {
    @Test
    public void testWithinTolerance(){
        var statistics = new Statistics();
        statistics.setToleranceMilliseconds(100D);
        statistics.setTargetMilliseconds(50D);

        var secureRandom = new SecureRandom();
        var sample = Double.valueOf(secureRandom.nextInt(99)+50);

        statistics.addSample(sample);

        assertEquals(statistics.speedWithinTolerance(),0,0);
    }

    @Test
    public void testAboveTolerance(){
        var statistics = new Statistics();
        statistics.setToleranceMilliseconds(100D);
        statistics.setTargetMilliseconds(50D);

        var secureRandom = new SecureRandom();
        for (int i = 0; i < 500; i++) {
            var sample = Double.valueOf(secureRandom.nextInt(99) + 160);
            statistics.addSample(sample);
        }

        assertEquals(statistics.speedWithinTolerance().doubleValue(),10d,99);
    }

    @Test
    public void testBeneathTolerance() {
        var statistics = new Statistics();
        statistics.setToleranceMilliseconds(100D);
        statistics.setTargetMilliseconds(550D);

        var secureRandom = new SecureRandom();
        for (int i = 0; i < 500; i++){
            var sample = Double.valueOf(secureRandom.nextInt(100));
            statistics.addSample(sample);
        }

        assertEquals(-450D,statistics.speedWithinTolerance().doubleValue(),99);
    }
    @Test
    public void testBeneathToleranceLargeSamples(){
        var statistics = new Statistics();
        statistics.setToleranceMilliseconds(100D);
        statistics.setTargetMilliseconds(400D);

        var secureRandom = new SecureRandom();
        for (int i = 0; i < 5000; i++) {
            var sample = Double.valueOf(secureRandom.nextInt(99) + 1260);
            statistics.addSample(sample);
        }
        for (int i = 0; i < 5000; i++) {
            var sample = Double.valueOf(secureRandom.nextInt(100))+101;
            statistics.addSample(sample);
        }
        assertEquals(-100d,statistics.speedWithinTolerance().doubleValue(),100);
    }
    @Test
    public void testAddSample(){
        var statistics = new Statistics();
        var start = System.nanoTime();
        System.out.println("do something..");
        statistics.addSample((System.nanoTime()-start)/1000000D);
        assertNotEquals(0,statistics.meanRelativeToTarget());

    }
}
