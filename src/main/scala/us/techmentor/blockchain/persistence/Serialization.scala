package us.techmentor.blockchain.persistence

import us.techmentor.blockchain.fact.Fact
import us.techmentor.blockchain.merkle.MerkleElement

trait MerkleTreeSerializationFunctions {
  type SerializeFunction = (SerializeFunctions, MerkleElement[Fact]) => Array[Byte];
  val noBytes = new Array[Byte](0);

  case class SerializeFunctions
  (
    node: SerializeFunction,
    element: SerializeFunction,
    children: SerializeFunction,
    factGroup: SerializeFunction,
    factElement: java.util.Map.Entry[String,String] => Array[Byte]
  )
}
