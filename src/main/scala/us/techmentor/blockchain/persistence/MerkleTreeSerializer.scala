package us.techmentor.blockchain.persistence

import us.techmentor.Numbers.intToByteArray
import us.techmentor.blockchain.fact.Fact
import us.techmentor.blockchain.merkle.{MerkleElement, MerkleTree}

import java.util.Optional
import scala.jdk.OptionConverters.RichOptional
import scala.jdk.javaapi.CollectionConverters.asScala

/**
 * node       := element-size{element}{children}
 * element    := {hash}:{factgroup}
 * hash       := 0-9, a-Z
 * factgroup  := fact-element-count{factelement}*
 * factelement:= fact-element-size{name}={value}
 * name,value := 0-9, a-Z
 * children   := node-count{node-size{node}}*
 */
class MerkleTreeSerializer extends Serializer[MerkleTree[Fact]]
                              with MerkleTreeSerializationFunctions {

  val children: SerializeFunction = (p,m) =>{
    def howMany(in: Array[Byte]) = if(in.length>0) 1 else 0;

    val l:Array[Byte] = childExpansion(p,m.left)
    val r:Array[Byte] = childExpansion(p,m.right)
    val cnt = howMany(l) + howMany(r)

    val result = cnt.toByte +: Array.concat(intToByteArray(l.length),l,intToByteArray(r.length), r)
    result
  }

  val factElement: java.util.Map.Entry[String,String] => Array[Byte] =
  e => {
    val bytes: Array[Byte] = (e.getKey+"="+e.getValue).getBytes()
    Array.concat(intToByteArray(bytes.length),bytes)
  }

  val factGroup: SerializeFunction = (p,m) => {
    val result = m.value.toScala.map(v=>{
      v.getValueMap.size().asInstanceOf[Byte] +:
        asScala(v.getValueMap.entrySet()).toArray
          .flatMap(e=>p.factElement(e))
    }).getOrElse(Array[Byte](0))
    result
  }

  val element: SerializeFunction = (p,m) =>{
    val result = m.hash.get().getValue.getBytes()++":".getBytes()++p.factGroup(p,m)
    result
  }

  val node: SerializeFunction = (p,m) => {
    val e = p.element(p,m)
    val result = (intToByteArray(e.length) ++ e) ++ p.children(p,m)
    result
  }

  def childExpansion(p: SerializeFunctions, child: Optional[MerkleElement[Fact]]) =
    child.toScala.map(p.node(p,_)).getOrElse(Array[Byte]())

  var serializeFunctions = SerializeFunctions(node,element,children,factGroup,factElement);

  def asBytes(merkleTree: MerkleTree[Fact]) = serializeFunctions.node(serializeFunctions,merkleTree.getRoot());
}
