package us.techmentor;

import org.apache.http.util.ByteArrayBuffer;

import java.util.List;

public class ByteArrayUtilities {
    public byte[][] split(byte[] input, byte splitOn){
        var buffer = new byte[input.length+10];
        int bufferLength=0;
        for(int i=0; i<input.length; i++,bufferLength++){
            if(input[i]==splitOn) {
                var smallerSize = input.length - bufferLength - 1;
                var smaller = new byte[smallerSize];
                System.arraycopy(input,i+1,smaller,0,smallerSize);

                var split = split(smaller, splitOn);
                var result = new byte[split.length+1][];
                System.arraycopy(split,0,result,1,split.length);

                result[0]=new byte[bufferLength];
                System.arraycopy(buffer,0,result[0],0,bufferLength);
                return result;
            }
            else buffer[i]=input[i];
        }
        var result = new byte[1][];
        result[0]=new byte[bufferLength];
        System.arraycopy(buffer,0,result[0],0,bufferLength);
        return result;
    }
    public static byte[] prepareBytes(List<byte[]> arrays) {
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(0);
        for(var a : arrays){
            byteArrayBuffer.append(a,0,a.length);
        }
        return byteArrayBuffer.toByteArray();
    }
}
