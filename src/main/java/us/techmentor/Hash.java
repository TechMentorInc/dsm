package us.techmentor;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import org.apache.commons.codec.binary.Hex;

public class Hash implements java.io.Serializable {

    public Hash(){}
    
    /**
     *
     */
    private static final long serialVersionUID = 8040290087369970287L;
    protected String value;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Hash && ((Hash)obj).value.equals(this.value);
    }

    public BigInteger asBigInteger(){
        return new BigInteger(value, 16);
    }
    public String getValue() { return this.value; }

    public void setValue(String value) {
         this.value=value; 
        }

    public static Hash getOrCreate(Object o){
        try{
            Class<?> clz = o.getClass();
            Method[] m = clz.getMethods();
            for(int i=0; i<m.length; i++){
                if( m[i].getName().equals("getHash") && 
                    m[i].getReturnType().equals(Hash.class)){
                    return (Hash)m[i].invoke(o);
                }
            }
            return create(o);
        }catch(Exception e){ throw new RuntimeException(e); }
    }
    public static Hash create(Object o){
        try{
            var mdInstance = MessageDigest.getInstance("MD5");
            var hash = new Hash();
            var digest = mdInstance
                .digest(
                    o.toString().getBytes(
                    StandardCharsets.UTF_8));
                    
            hash.value =
                Hex.encodeHexString(digest);

            return hash;
        }catch(Throwable t){ throw new RuntimeException(t); }
    }
    public static Hash fromString(String str){
        return new Hash(){{
            this.value = str;
        }};
    }
}
