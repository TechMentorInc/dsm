package us.techmentor.network;

import java.io.IOException;
import java.io.InputStream;

public class NetworkUtilities {
    public static void wait(int length, int increment, InputStream input) throws IOException, InterruptedException {
        for(int i=0; i<length && input.available()==0; i++){
            if(input.available()>0) return;
            Thread.sleep(increment);
        }
    }
}
