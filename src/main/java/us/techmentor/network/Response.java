package us.techmentor.network;

import org.apache.commons.lang3.function.TriFunction;

import java.io.InputStream;
import java.io.OutputStream;

public record Response<T>(int secondsToWait, ResponseAction<T> action){
    public interface ResponseAction<T> extends TriFunction<byte[], InputStream, OutputStream, T> {};
}
