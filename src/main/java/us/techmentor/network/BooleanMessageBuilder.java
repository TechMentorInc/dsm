package us.techmentor.network;

import java.util.ArrayList;
import java.util.List;

public class BooleanMessageBuilder extends MessageBuilder<Boolean> {
    protected List<Response<Boolean>> responses = new ArrayList<Response<Boolean>>();

    @Override
    protected List getResponses(){return this.responses;}

    @Override
    public MessageBuilder<Boolean> awaitResponse(int secondsToWait, String expectedValue) {
        responses.add(new Response<>(secondsToWait, (b, input, output) -> new String(b).startsWith(expectedValue)));
        return this;
    }
}
