package us.techmentor.blockchain;

import us.techmentor.blockchain.fact.Fact;

public class PrettyPrint {
    public static String format(Chain chain){
        try {
            var blocks = chain.getBlocks();
            var result = new StringBuffer();
            result.append("Total Blocks in Chain: " + blocks.size() + "\n");
            for (int i = 0; i < blocks.size(); i++) {
                var block = blocks.get(i);
                result.append("Block #" + i + "\n");

                var blockhash = block.getHash() == null ? "null" : block.getHash().getValue();
                result.append("\tHash: " + blockhash + "\n");
                result.append("\tNonce: " + block.getNonce() + "\n");

                var previous = block.getPreviousBlockHash() == null ? "null" : block.getPreviousBlockHash().getValue();
                result.append("\tPrevious: " + previous + "\n");

                var facts = block.getFacts().asList();
                result.append("\n\tTotal Facts on Block: " + facts.size() + "\n");

                for (int j = 0; j < facts.size(); j++) {
                    var fact = facts.get(j);
                    result.append("\tFact #" + j + "\n");

                    result.append(format(fact));
                }
                result.append("\n\n");
            }
            return result.toString();
        }catch (Throwable t){
            return "Error assembling prettyPrint of chain: "+t.getMessage();
        }
    }

    public static String format(Fact fact){
        try {
            var result = new StringBuffer();
            var facthash = fact.getHash() == null ? "null" : fact.getHash().getValue();
            result.append("\t\tHash: " + facthash + "\n");

            var factMap = fact.getValueMap();
            var factKeys = factMap.keySet().toArray();
            result.append("\t\tValues\n");
            for (int k = 0; k < factKeys.length; k++) {
                var key = factKeys[k];
                var value = factMap.get(key);
                result.append("\t\t\t" + key + ": " + value + "\n");
            }
            return result.toString();
        }catch(Throwable t){
            return "Error assembling prettyPrint of fact: "+t.getMessage();
        }
    }
}
