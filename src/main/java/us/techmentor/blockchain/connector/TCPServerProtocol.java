package us.techmentor.blockchain.connector;

import us.techmentor.blockchain.Chain;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;
import us.techmentor.blockchain.fact.Fact;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class TCPServerProtocol implements ServerProtocol{

    Queue<Fact> incomingFactQueue = new LinkedBlockingQueue<>();
    Queue<Chain> incomingChainQueue = new LinkedBlockingQueue<>();

    Queue<Fact> outgoingFactQueue = new LinkedBlockingQueue<>();
    Queue<Chain> outgoingChainQueue = new LinkedBlockingQueue<>();

    Queue<ComplexityProposal> incomingComplexityQueue = new LinkedBlockingQueue<>();
    Queue<ComplexityProposal> outgoingComplexityQueue = new LinkedBlockingQueue<>();

    @Override
    public boolean register(){
        return false;
    }

    @Override
    public void sendFact(Fact fact) {
        outgoingFactQueue.add(fact);
    }

    @Override
    public Optional<Fact> pollIncomingFact() {
        if(!incomingFactQueue.isEmpty())
            return Optional.of(incomingFactQueue.remove());
        return Optional.empty();
    }

    @Override
    public Optional<Chain> pollIncomingChain() {
        if(!incomingChainQueue.isEmpty())
            return Optional.of(incomingChainQueue.remove());
        return Optional.empty();
    }

    @Override
    public Optional<ComplexityProposal> pollIncomingComplexity() {
        if(!incomingComplexityQueue.isEmpty())
            return Optional.of(incomingComplexityQueue.remove());
        return Optional.empty();
    }

    @Override public void updateChain(Chain chain) {
        outgoingChainQueue.add(chain);
    }
    @Override public void sendComplexity(ComplexityProposal complexityProposal) {
        outgoingComplexityQueue.add(complexityProposal);
    }
    @Override public void addFactToIncomingQueue(Fact fact) {
        incomingFactQueue.add(fact);
    }
    @Override public void addChainToIncomingQueue(Chain chain) {
        incomingChainQueue.add(chain);
    }
    @Override public void addComplexitySuggestionToIncomingQueue(ComplexityProposal complexityProposal) {
        incomingComplexityQueue.add(complexityProposal);
    }
    @Override public Optional<Fact> getNextFactFromOutgoingQueue() {
        if(!outgoingFactQueue.isEmpty())
            return Optional.of(outgoingFactQueue.remove());
        return Optional.empty();
    }

    @Override
    public Optional<Chain> getNextChainFromOutgoingQueue() {
        if(!outgoingChainQueue.isEmpty())
            return Optional.of(outgoingChainQueue.remove());
        return Optional.empty();
    }

    @Override
    public Optional<ComplexityProposal> getNextComplexityProposalFromOutgoingQueue() {
        if(!outgoingComplexityQueue.isEmpty())
            return Optional.of(outgoingComplexityQueue.remove());
        return Optional.empty();
    }
}
