package us.techmentor.blockchain.connector;

import com.google.inject.Inject;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import us.techmentor.blockchain.Chain;
import us.techmentor.blockchain.PrettyPrint;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.persistence.ChainSerializer;
import us.techmentor.dsm.remotes.FactSerializer;
import us.techmentor.network.MessageBuilder;

import java.net.ConnectException;
import java.net.Socket;
import java.util.Optional;

public class TCPClientProtocol implements Protocol {
    @Inject FactSerializer factSerializer;
    @Inject ChainSerializer chainSerializer;

    String ipAddress;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public String getIpAddress(){ return this.ipAddress; }
    public void setIpAddress(String ipAddress){ this.ipAddress = ipAddress; }

    @Override
    public void sendFact(Fact fact) {
        try {
            MDC.put("category","network");
            logger.info("tcpSendFact (peer={}),fact=\n{}",this.ipAddress,PrettyPrint.format(fact));
            var clientSocket = new Socket(this.ipAddress,6665);
            var bytes = factSerializer.asBytes(fact);
            logger.info("tcpSendFact (peer={}),fact bytes=\n{}",this.ipAddress,new String(bytes));
            var out = clientSocket.getOutputStream();
            out.write(9);
            out.write("TAKE FACT".getBytes());
            out.write(bytes);
            clientSocket.close();
        } catch (Throwable t) {
            logger.error("Error received (peer={}), tcpSendFact: {}",this.ipAddress,ExceptionUtils.getStackTrace(t));
        } finally { MDC.clear(); }
    }

    @Override
    public Optional<Fact> pollIncomingFact() {
        var giveFact = "GIVE FACT".getBytes();
        return MessageBuilder.createWithResult(Fact.class)
            .addMessage((byte)giveFact.length)
            .addMessage(giveFact)
            .awaitResponse(10, (b,i,o)-> factSerializer.asFact(b))
            .sendTCP(this.ipAddress,6665,MessageBuilder.FIRST_ELEMENT);
    }

    @Override
    public Optional<Chain> pollIncomingChain() {
        try{
            var giveChain = "GIVE CHAIN".getBytes();
            MDC.put("category","network");
            logger.info("Issuing Give Chain...peer={}",this.ipAddress);
            var result = MessageBuilder.createWithResult(Chain.class)
                    .addMessage((byte)giveChain.length)
                    .addMessage(giveChain)
                    .awaitResponse(10, (b,i,o)-> chainSerializer.fromBytes(b))
                    .sendTCP(this.ipAddress,6665,MessageBuilder.FIRST_ELEMENT);
            logger.info("Received Chain Bytes (peer={}), Success = {}",this.ipAddress,result);
            return result;
        }finally{MDC.clear();}
    }

    @Override
    public void updateChain(Chain chain){
        try{
            MDC.put("category","network");
            var chainBytes = chainSerializer.toBytes(chain);
            logger.info("Pushing chain to peer! Peer={}, Chain={}",this.ipAddress,new String(chainBytes));
            var result = MessageBuilder.createWithResult(Boolean.class)
                .addMessage((byte)10)
                .addMessage("TAKE CHAIN")
                .addMessage(chainBytes)
                .sendTCP(this.ipAddress,6665,MessageBuilder.BOOLEAN_SUM);
            logger.info("Chain push complete! Peer={}, Success={}",this.ipAddress,result);
        }finally{MDC.clear();}
    }

    @Override
    public boolean register(){
        MDC.put("category","network");
        try{
            var registerMe = "REGISTER ME".getBytes();
            logger.info("Seeking registration! Peer={}",this.ipAddress);
            var builder = MessageBuilder.createWithResult(Boolean.class);
            Boolean result =
                builder
                    .addMessage((byte)registerMe.length)
                    .addMessage(registerMe)
                    .awaitResponse(10,"REGISTERED [")
                    .sendTCP(this.ipAddress,6665,MessageBuilder.BOOLEAN_SUM)
                    .get();
            logger.info("Registered (peer={}) result={}",this.ipAddress,result);
            return result;
        } finally{ MDC.clear(); }
    }

    @Override
    public void sendComplexity(ComplexityProposal complexityProposal) {
        try {
            MDC.put("category","network");
            logger.info("Sending complexity! peer={}",this.ipAddress);
            var socket = new Socket(this.ipAddress, 6665);
            var output = socket.getOutputStream();


            var header = "TAKE COMPLEXITY".getBytes();
            output.write(header.length);
            output.write(header);
            output.write(("\n"+complexityProposal.toString()).getBytes());
            socket.close();

            logger.info("Complexity (peer={}) Sent: {}",this.ipAddress,complexityProposal);
        }catch(Throwable t)
        {
            String stacktrace = ExceptionUtils.getStackTrace(t);
            logger.error("Failed Complexity Suggestion (peer={})...\n{}",this.ipAddress,stacktrace);
        }finally { MDC.clear(); }
    }

    @Override
    public Optional<ComplexityProposal> pollIncomingComplexity() {
        try {
            MDC.put("category","network");
            logger.info("Requesting complexity! peer={}",this.ipAddress);
            var socket = new Socket(this.ipAddress, 6665);
            var output = socket.getOutputStream();
            var input = socket.getInputStream();

            var header = "GIVE COMPLEXITY".getBytes();
            output.write(header.length);
            output.write(header);

            for(int i=0; i<10 && input.available()==0; i++){Thread.sleep(100);}
            if(input.available()==0){
                logger.info("No result from give complexity.");
                return Optional.empty();
            }
            var inBytes = new byte[input.available()];
            input.read(inBytes);
            socket.close();

            logger.info("Complexity Received (peer={}): {}",this.ipAddress,new String(inBytes));
            return Optional.of(ComplexityProposal.fromString(new String(inBytes)));
        }catch(Throwable t)
        {
            String stacktrace = ExceptionUtils.getStackTrace(t);
            logger.error("Failed To Seek Complexity Advice (peer={})...\n{}",this.ipAddress,stacktrace);
        }finally{ MDC.clear(); }
        return Optional.empty();
    }
}
