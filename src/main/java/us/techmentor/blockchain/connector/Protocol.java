package us.techmentor.blockchain.connector;

import java.util.Optional;

import us.techmentor.blockchain.Chain;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;

public interface Protocol {
	boolean register();

	void sendFact(Fact fact);
	Optional<Fact> pollIncomingFact();

	void updateChain(Chain chain);
	Optional<Chain> pollIncomingChain();

    void sendComplexity(ComplexityProposal complexityProposal);
	Optional<ComplexityProposal> pollIncomingComplexity();
}
