package us.techmentor.blockchain.fact;

import java.util.List;

import org.joda.time.DateTime;

import us.techmentor.blockchain.fact.FactQuery.Truth;

public class Truths {
    public static Truth Since(DateTime value) {
        return new Truth((Object o1,Object o2)->{
                var d1 = (DateTime)o1;
                var d2 = convertToDateTime(o2);
                if(d2==null) return false;
                return d2.isAfter(d1);
            },(Object)value
        );
    }   
    private static DateTime convertToDateTime(Object o){
        if(o instanceof String)
            return DateTime.parse((String)o);
        else if(o instanceof DateTime)
            return (DateTime)o;
        return null;
    }
    
    @SuppressWarnings("unchecked")
    public static Truth In(List<String> set){
        return new Truth(
            (Object o1, Object o2) ->{
                var listOfStrings = (List<String>)o1;
                return listOfStrings.contains(o2);
            }, (Object)set
        );
    }
}
