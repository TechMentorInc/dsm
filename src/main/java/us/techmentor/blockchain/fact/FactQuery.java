package us.techmentor.blockchain.fact;

import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

public class FactQuery {
    private Map<String,Truth> query;
    public FactQuery(Map<String,Truth> query){
        this.query = query;
    }
    public BiFunction<Object,Object,Boolean> getComparison(String key){
        return ((Truth)query.get(key)).comparison;
    }
    public Object getValue(String key){
        return ((Truth)query.get(key)).value;
    }
    public Set<String> getKeys(){
        return query.keySet();
    }
    public static class Truth{
        public Truth(BiFunction<Object,Object,Boolean> comparison, Object value){
            this.value=value;
            this.comparison=comparison;
        }
        Object value;
        BiFunction<Object,Object,Boolean> comparison;
    }
    public static class StringTruth extends Truth{
        public StringTruth(String value){
            super(stringEquals,value);
        }
    }
    public static BiFunction<Object,Object,Boolean> stringEquals = (Object s1, Object s2)->{
        return s1.equals(s2);
    };
}
