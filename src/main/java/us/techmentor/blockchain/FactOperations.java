package us.techmentor.blockchain;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;
import us.techmentor.blockchain.speed.Statistics;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static us.techmentor.blockchain.PrettyPrint.format;

public class FactOperations {
    @Inject Blockchain blockchain;
    @Inject BlockPreparation blockPreparation;
    @Inject Statistics statistics;

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public void blockUntilFactOnChain(Fact fact) throws InterruptedException {
        var cnt = 0;

        logger.info("Blocking on fact: \n"+format(fact));
        while(!blockchain.chain.contains(fact)){
            cnt++;
            Thread.sleep(500);
            if(cnt%20==0)
                logger.info("Still waiting ... chain: \n"+format(blockchain.chain)+"\n\nfact: \n"+format(fact));
        }
        float seconds = cnt;
        seconds /= 2;
        logger.info("Waited for "+seconds+" seconds for the following fact: \n"+format(fact));
    }
    public void saveFact(Fact fact){
        saveFact(fact,(b)->{});
    }
    public void saveFact(Fact fact, Consumer<Block> finished){
        var start = System.nanoTime();
        logger.info("<%s> Save Fact [%s] [%s]".formatted(Thread.currentThread().getName(),fact,fact.getValueMap()));

        synchronized(blockchain.chain){
            if(blockPreparation.isEnqueued(fact) || blockchain.chain.contains(fact)) {
                logger.info("Fact already handled ["+fact+"]");
                return;
            }
            blockchain.blockchainConnectors.stream().forEach(c -> c.sendFact(fact));

            logger.info("<%s> Queuing Fact [%s] [%s]".formatted(Thread.currentThread().getName(),fact,fact.getValueMap()));
            blockPreparation.addFactToQueue(fact, blockchain.chain,
            b -> {
                synchronized(blockchain.chain){
                    if(blockchain.chain.contains(fact)) return;
                    if(!blockchain.chain.validNextBlock(b) ||
                    b.facts
                        .asList()
                        .stream()
                        .map(f->blockchain.chain.contains(f))
                        .reduce(false, (accumulation,value)->(accumulation||value))){

                        var strayFacts = b.factsNotIn(blockchain.chain);
                        strayFacts.forEach(f->saveFact(f));
                        logger.info("Failed block");
                        return;
                    }

                    blockchain.chain.add(b);
                    logBlock(b);
                    blockchain.blockchainConnectors.stream().forEach(c->c.updateChain(blockchain.chain));
                }

                statistics.addSample((System.nanoTime()-start)/1000000D);
                finished.accept(b);
            });
        }
    }

    public Optional<Fact> readFact(FactQuery factQuery) {
        return blockchain.chain
                .locateFacts(factQuery)
                .stream()
                .reduce((a,b)->b);
    }
    public List<Fact> readFacts(FactQuery factQuery){
        return blockchain.chain.locateFacts(factQuery);
    }

    private void logBlock(Block b){
        logger.info("<%s> Facts on block <%s>: <%s>".formatted(
            Thread.currentThread().getName(),
            b,
            b.facts.asList().stream().map(f->f.getValueMap()).collect(Collectors.toList())));
    }
}
