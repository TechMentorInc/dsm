package us.techmentor.blockchain.speed;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.BlockPreparation;
import us.techmentor.blockchain.speed.complexity.Complexity;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class Proposition {
    @Inject SpeedAdaptingConnectorManager speedAdaptingConnectorManager;
    @Inject Complexity complexity;
    @Inject BlockPreparation blockPreparation;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Optional<ComplexityProposal> increaseSpeed()
    { return changeSpeed(complexity.decreasedComplexity(blockPreparation.getComplexity())); }

    public Optional<ComplexityProposal> decreaseSpeed()
    { return changeSpeed(complexity.increasedComplexity(blockPreparation.getComplexity())); }

    private Optional<ComplexityProposal> changeSpeed(ComplexityDefinition complexityDefinition) {
        logger.info("Accessing Speed Force To Modify Speed");

        var connectors = speedAdaptingConnectorManager.getSpeedAdaptingConnectors();
        logger.info("Speed change connectors={}",connectors.length);

        var complexityProposal = complexity.createNoActionProposal(complexityDefinition);
        List<Boolean> results = new ArrayList();
        Runnable success = ()->results.add(true);
        Runnable failure = ()->results.add(false);

        for(int i=0; i<connectors.length; i++)
            connectors[i].proposeComplexity(complexityProposal, success, failure);

        waitForResults(results, connectors.length, 100, TimeUnit.SECONDS);
        var result = results.size()==connectors.length && results.stream().reduce(true,(a,b)->a&&b);
        logger.info("Speed Increase Result: [count={},result={}/{}]",connectors.length,result,results.size());

        return result ? Optional.of(complexityProposal): Optional.empty();
    }

    private void waitForResults(List<Boolean> results, int lengthAwaited, int time, TimeUnit units){
        for(int i=0; i<10; i++){
            if(results.size()<lengthAwaited)
                try {
                    Thread.sleep(units.toMillis(time/10));
                }catch(Throwable t){}
        }
    }
}
