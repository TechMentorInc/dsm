package us.techmentor.blockchain.speed;


import com.google.inject.ImplementedBy;
import us.techmentor.blockchain.Blockchain;

@ImplementedBy(Blockchain.class)
public interface SpeedAdaptingConnectorManager {
    SpeedAdaptingConnector[] getSpeedAdaptingConnectors();
}
