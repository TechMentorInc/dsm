package us.techmentor.blockchain.speed.complexity;

public enum Action {
    NONE("None"),
    PROPOSE("Propose"),
    ACCEPT("Accept");

    public final String label;

    private Action(String label) {
        this.label = label;
    }
    public static Action valueOfLabel(String label) {
        for (Action e : values()) {
            if (e.label.equals(label)) {
                return e;
            }
        }
        return null;
    }
}
