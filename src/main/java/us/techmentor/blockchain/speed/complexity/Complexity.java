package us.techmentor.blockchain.speed.complexity;

import org.joda.time.DateTime;
import us.techmentor.Hash;

import java.math.BigInteger;

public class Complexity {

    public ComplexityDefinition increasedComplexity(ComplexityDefinition currentComplexityDefinition){
        return new ComplexityDefinition(currentComplexityDefinition.nonceTarget().shiftRight(4));
    }
    public ComplexityDefinition decreasedComplexity(ComplexityDefinition currentComplexityDefinition){
        return new ComplexityDefinition(currentComplexityDefinition.nonceTarget().shiftLeft(4).or(BigInteger.valueOf(0xf)));
    }

    public ComplexityProposal createNoActionProposal(ComplexityDefinition complexityDefinition) {
        var time = DateTime.now();
        var id = Hash.create("["+time.getMillis()+"-"+complexityDefinition.toString()+"]");
        return new ComplexityProposal(id, Action.NONE, time, complexityDefinition);
    }
}
