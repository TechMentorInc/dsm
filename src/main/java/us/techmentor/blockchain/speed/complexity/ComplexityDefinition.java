package us.techmentor.blockchain.speed.complexity;

import java.math.BigInteger;

public record ComplexityDefinition(BigInteger nonceTarget) {}
