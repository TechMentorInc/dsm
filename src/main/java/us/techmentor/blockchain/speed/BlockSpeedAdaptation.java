package us.techmentor.blockchain.speed;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.BlockPreparation;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Singleton
public class BlockSpeedAdaptation {
    @Inject Proposition proposition;
    @Inject BlockPreparation blockPreparation;
    @Inject Statistics statistics;

    private boolean started = false;
    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public void start(){
        if(started) return;
        started = true;
        statistics.setTargetMilliseconds(1000D);
        statistics.setToleranceMilliseconds(50D);
        executorService.scheduleAtFixedRate(
            ()->this.check(),
            1000L,1000L,
            TimeUnit.MILLISECONDS);
    }

    void check(){
        statistics.logIt();
        var variation = statistics.speedWithinTolerance();
        logger.info("Variation="+variation);
        if(variation<0) increaseSpeed();
        else if(variation>0) decreaseSpeed();
    }

    void increaseSpeed(){
        proposition
        .increaseSpeed()
        .ifPresent(proposal-> blockPreparation.setComplexity(proposal.complexityDefinition()));
    }

    void decreaseSpeed(){
        proposition
        .decreaseSpeed()
        .ifPresent(proposal-> blockPreparation.setComplexity(proposal.complexityDefinition()));
    }
    public void stop(){
        executorService.shutdownNow();
    }
}
