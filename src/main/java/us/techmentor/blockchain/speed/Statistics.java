package us.techmentor.blockchain.speed;

import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Singleton
public class Statistics {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    Double target=0D,tolerance=0D;
    public Double speedWithinTolerance() {
        var top = target+tolerance;
        var bottom = target-tolerance;
        if(mean>top)
            return mean-top;
        else if(mean<bottom) {
            return mean - bottom;
        }
        return 0D;
    }

    public void setTargetMilliseconds(Double target){ this.target = target; }
    public void setToleranceMilliseconds(Double tolerance){ this.tolerance = tolerance;}

    List<Double> samples = new ArrayList<Double>();
    Double mean = 0D;
    public void addSample(Double sample) {
        samples.add(sample);
        calculateMean();
        while(samples.size()>2000) samples.remove(0);
    }
    public void calculateMean(){
        var i=0;
        var total = 0D;
        for(; i<samples.size(); i++) total+=samples.get(i);
        this.mean = total/i;
    }

    public Double meanRelativeToTarget() {
        calculateMean();
        return mean - target;
    }

    public void logIt() {
        logger.info("target="+target+" | tolerance="+tolerance+" | samples="+samples.size()+" | mean="+this.mean);
    }
}
