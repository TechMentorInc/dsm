package us.techmentor.blockchain;

import java.util.*;

import java.util.stream.Collectors;

import us.techmentor.Hash;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;

public class Chain {
	private List<Block> chain = new ArrayList<>();

    @Override
	public synchronized Object clone(){
		var chain = new Chain();
		chain.chain = new ArrayList<>(this.chain);
		return chain;
	}

	public synchronized boolean validate(){
        for(int i=0; i<chain.size(); i++) {
            var current = chain.get(i);
			if(i>0){
				var previous = chain.get(i-1);
				if(!previous.hash.equals(current.previousBlockHash)) return false;
			}
			if(!current.facts.validate()) return false;
			if(!current.validate()) return false;
        }
		return true;
	}
	public synchronized boolean validNextBlock(Block b){
		return chain.size()==0 || chain.get(chain.size()-1).hash.equals(b.previousBlockHash);
	}

	public synchronized void add(Block block) {
        chain.add(block);
	}

	Boolean compareQueryToFact(FactQuery factQuery, Fact fact){
		Set<String> keys = factQuery.getKeys();
		return 
			keys
			.stream()
			.map(key -> 
			factQuery
				.getComparison(key)
				.apply(
					factQuery.getValue(key),
					fact.get(key)
				)
			)
			.reduce(true, (overall,element) -> overall&&element);
	}

	List<Fact> find(FactQuery factQuery, Block block) {
		return block.getFacts().asList().stream()
			.filter(f -> compareQueryToFact(factQuery,f))
			.collect(Collectors.toList());
	}

	public synchronized List<Fact> locateFacts(FactQuery factQuery) {
		return chain
			.stream()
			.flatMap(b -> find(factQuery,b).stream())
			.collect(Collectors.toList());
	}

	public synchronized List<Fact> factsNotIn(Chain in) {
		var localFacts = 
			this.chain.stream()
			.flatMap(b -> b.facts.asList().stream())
			.collect(Collectors.toList());
		
		var inFacts = 
			in.chain.stream()
			.flatMap(b -> b.facts.asList().stream())
			.collect(Collectors.toList());

		return localFacts.stream()
			.filter(f -> !inFacts.contains(f))
			.collect(Collectors.toList());
	}

	public synchronized boolean contains(Fact fact) {
		for (int i=0; i<chain.size(); i++)
			if(chain.get(i).facts.asList().contains(fact))
				return true;
		return false;
	}

    public synchronized List<Block> getBlocks() { return this.chain; }

	public synchronized void sort(){
		var result = new ArrayList<Block>();
		var current = chain.stream().filter(p->p.previousBlockHash==null).findFirst().get();
		while (result.size()<chain.size()){
			result.add(current);
			if(result.size()==chain.size()) continue;

			var currentHash = current.hash;
			var newCurrent = chain.stream().filter(
				p -> p.previousBlockHash!=null && p.previousBlockHash.getValue().equals(currentHash.getValue())
			).findFirst();

			if(newCurrent.isEmpty())
				continue;
			current = newCurrent.get();
		}
		chain = result;
	}

	public synchronized int getChainLength() {
		return chain.size();
	}

	public synchronized void commonIndex(Chain comparisonChain, ChainComparator.Comparison result) {
		synchronized (comparisonChain){
			var i1 = this.chain.iterator();
			var i2 = comparisonChain.chain.iterator();

			int commonIndex = 0;
			boolean common = true;

			while(i1.hasNext() && i2.hasNext() && common){
				var block1 = i1.next();
				var block2 = i2.next();
				if(block1.equals(block2)) commonIndex++;
				else common=false;
			}
			result.lastCommonIndex = commonIndex-1;
		}
	}

	public synchronized List<Fact> getAllFacts() {
		return this.chain.stream()
				.flatMap(b -> b.facts.asList().stream())
				.collect(Collectors.toList());
	}

	public synchronized Hash getLastHash() {
		return this.chain.get(this.chain.size() - 1).getHash();
	}
}
