package us.techmentor.blockchain;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.persistence.BlockchainPersistence;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static us.techmentor.blockchain.PrettyPrint.format;

public class ChainOperations {
    @Inject Blockchain blockchain;
    @Inject FactOperations factOperations;
    @Inject BlockchainPersistence blockchainPersistence;

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public void updateChain(Chain chain){
        var caller = new Throwable().getStackTrace()[1];
        logger.info("<%s> Starting updateChain(), caller=%s".formatted(Thread.currentThread().getName(),caller));

        var results = new ChainComparator().compare(chain,blockchain.chain);
        logger.info("<%s> updateChain() comparison results: \n[%s]"
                .formatted(
                        Thread.currentThread().getName(),
                        results)
        );

        Chain newChain = null;
        Chain oldChain = null;
        if(!results.equalLength){
            newChain = results.longer;
            oldChain = results.shorter;
        }
        else if(results.equalLength && results.lastCommonIndex < chain.getChainLength()-1) {
            newChain = chain;
            oldChain = blockchain.chain;
        }

        if(newChain!=null){
            logger.info("<%s> Final Updated Chain: \n\n%s".formatted(
                    Thread.currentThread().getName(),
                    format(newChain)));

            var extraFacts = oldChain.factsNotIn(newChain);
            blockchain.chain = newChain;
            blockchain.blockchainConnectors.stream().forEach(a->a.updateChain(blockchain.chain));
            extraFacts.forEach(f->factOperations.saveFact(f));
        }
    }

    ScheduledExecutorService stateManagementExecutorService
            = Executors.newSingleThreadScheduledExecutor();
    public void load(){ this.updateChain(blockchainPersistence.load()); }
    public void save(){ blockchainPersistence.save(blockchain); }
    public void initiateRegularLoadAndSave(){
        load();
        stateManagementExecutorService
                .scheduleAtFixedRate(()->{
                    logger.info("Regularly scheduled chain loading.");
                    load();

                    logger.info("Regularly scheduled chain saving.");
                    save();
                }, 100, 500, TimeUnit.MILLISECONDS);
    }

    public Chain getChain() {
        return this.blockchain.chain;
    }
    public void stop(){
        this.stateManagementExecutorService.shutdownNow();
    }
}
