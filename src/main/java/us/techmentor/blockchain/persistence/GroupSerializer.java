package us.techmentor.blockchain.persistence;

import java.util.Collection;

import org.apache.commons.lang3.ArrayUtils;


public abstract class GroupSerializer<T> implements Serializer<Collection<T>>{
    @Override
    public byte[] asBytes(Collection<T> collection){
        return
            collection.stream().map(a-> getSerializer().asBytes(a)).reduce((a,b)->{
                return ArrayUtils.addAll(a,b);
            }).get();
    }
    protected abstract Serializer<T> getSerializer();
}