package us.techmentor.blockchain.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.inject.Inject;

import com.google.inject.Injector;
import us.techmentor.blockchain.Blockchain;
import us.techmentor.blockchain.Chain;
import static us.techmentor.Numbers.*;

public class ChainSerializer {

    @Inject
    BlockSerializer blockSerializer;
    @Inject
    Injector injector;

    public byte[] toBytes(Chain chain){
        return serializeChain(chain)
            .stream()
            .map(f->f.data)
            .reduce(
            new byte[0],
            (a,b) -> {
                byte[] c = new byte[a.length + b.length + 4];
                System.arraycopy(a, 0, c, 0, a.length);
                System.arraycopy(intToByteArray(b.length),0,c,a.length,4);
                System.arraycopy(b, 0, c, a.length+4, b.length);
                return c;
            });
    }
    public Chain fromBytes(byte[] bytes){
        List<PreparedBlock> blocks = new ArrayList<>();
        for(int index = 0;index<bytes.length;){
            int size = byteArrayToInt(bytes,index);
            var blockData = new byte[size];
            System.arraycopy(bytes,index+4,blockData,0,size);
            blocks.add(new PreparedBlock(){{
                this.data=blockData;
            }});
            index+=size+4;
        }
        return deserializeChain(blocks);
    }
    public List<PreparedBlock> serializeChain(Blockchain chain){
        return serializeChain(chain.getChain());
    }
    public List<PreparedBlock> serializeChain(Chain chain){
        synchronized(chain){
            return chain
                    .getBlocks()
                    .stream()
                    .map((b)-> new PreparedBlock(){{
                        this.data = blockSerializer.asBytes(b);
                        this.block = b;
                    }})
                    .collect(Collectors.toList());
        }
    }

    public Chain deserializeChain(List<PreparedBlock> blocks){
        if(blocks==null || blocks.size()==0) return new Chain();
        var chain = new Chain();
        blocks.forEach(block->{
            chain.add(blockSerializer.asBlock(block.data));
        });
        chain.sort();
        return chain;
    }
}
