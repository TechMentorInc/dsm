package us.techmentor.blockchain.persistence;

import com.google.inject.Inject;

import org.apache.commons.lang3.ArrayUtils;

import us.techmentor.ByteArrayUtilities;
import us.techmentor.Hash;
import us.techmentor.blockchain.Block;

import java.math.BigInteger;

public class BlockSerializer implements Serializer<Block>{

    @Inject public MerkleTreeSerializer merkleTreeSerializer;
    @Inject public MerkleTreeDeserializer merkleTreeDeserializer;
    @Inject public ByteArrayUtilities byteArrayUtilities;

    @Override
    public byte[] asBytes(Block block) {
        var previousBlock = block.getPreviousBlockHash()==null?null:block.getPreviousBlockHash().getValue();
        return
        ArrayUtils.addAll(
            (
                block.getHash().getValue()+","+
                block.getFacts().getRoot().hash.get().getValue()+","+
                previousBlock+","+
                block.getNonce()+","
            ).getBytes(),
            merkleTreeSerializer.asBytes(block.getFacts()));
    }

    public Block asBlock(byte[] bytes){
        var block = new Block();
        var tree = byteArrayUtilities.split(bytes,(byte)',')[4];
        var hash = byteArrayUtilities.split(bytes,(byte)',')[0];
        var previousBlockHash = byteArrayUtilities.split(bytes,(byte)',')[2];
        var nonce = byteArrayUtilities.split(bytes,(byte)',')[3];

        block.setNonce(new BigInteger(new String(nonce)));

        if(new String(previousBlockHash).equals("null")) {
            block.setPreviousBlockHash(null);
        }else {
            block.setPreviousBlockHash(new Hash() {{
                this.setValue(new String(previousBlockHash));
            }});
        }
        block.setHash(new Hash(){{
            this.setValue(new String(hash));
        }});
        block.setFacts(merkleTreeDeserializer.asTree(tree));
        return block;
    }
}
