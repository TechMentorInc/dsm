package us.techmentor.blockchain.persistence;

import com.google.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.Blockchain;
import us.techmentor.blockchain.Chain;

public class BlockchainPersistence {

    @Inject
    ChainSerializer chainSerializer;

    @Inject
    DiskPersistence diskPersistence;

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    public void save(Blockchain blockchain) {
        logger.info("Saving blockchain...{}", blockchain.getChain().getBlocks().size());
        diskPersistence.commit(
            chainSerializer.serializeChain(
                blockchain
            )
        );
    }

    public Chain load() {
        return chainSerializer.deserializeChain(
          diskPersistence.retrieve()
        );
    }

}
