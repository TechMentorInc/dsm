package us.techmentor.blockchain.persistence;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.dsm.metadata.MetaDataOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DiskPersistence {
    @Inject
    MetaDataOperations metaDataOperations;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    public void commit(List<PreparedBlock> blocks) {
        logger.info("Committing blocks to disk: {}", blocks.size());
        var dsmPath = getPathAsFile("chain");
        blocks.forEach(block->{
            var file = new File(dsmPath,block.getBlock().getHash().getValue());
            if(!file.exists()){
                try {
                    file.createNewFile();
                    var fos = new FileOutputStream(file);
                    fos.write(block.getData());
                    fos.close();
                }
                catch(IOException  ioe){ throw new RuntimeException(ioe); }
            }
        });
    }

    public List<PreparedBlock> retrieve(){
        var dsmPath = getPathAsFile("chain");
        var files = dsmPath.listFiles();
        return Arrays.stream(files).map(file -> {
            try {
                var fis = new FileInputStream(file);
                var bytes = new byte[fis.available()];
                fis.read(bytes);
                var preparedBlock = new PreparedBlock();
                preparedBlock.data = bytes;
                return preparedBlock;
            }
            catch(Exception e){throw new RuntimeException(e);}

        }).collect(Collectors.toList());
    }

    File getPathAsFile(String path){
        var processedPath = Paths.get(metaDataOperations.getMetaData().dsmHomeDirectory().toString(),path);
        if(!processedPath.toFile().exists())
            processedPath.toFile().mkdir();
        return processedPath.toFile();
    }
}
