package us.techmentor.blockchain;

import java.math.BigInteger;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.Hash;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.speed.complexity.ComplexityDefinition;

@Singleton
public class BlockPreparation {
    ExecutorService executorService = Executors.newFixedThreadPool(1);
    Queue<Fact> queue = new LinkedBlockingQueue<Fact>();
    BigInteger maximum =   new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",16);
    public BigInteger target =    new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",16);
    Logger logger = LoggerFactory.getLogger(this.getClass().getName());
    boolean solving=false;

    public void addFactToQueue(Fact fact, Chain chain, Consumer<Block> callback){
        logger.info("Waiting for queue... ["+fact+"]");
        synchronized(queue){
            logger.info("Checking for fact ["+fact+"]");
            if(!queue.contains(fact)){
                logger.info("Adding fact ["+fact+"]");
                queue.add(fact);
                if(!solving){
                    solving=true;
                    logger.info("Solving for block ["+fact+"]");
                    solveBlock(chain,callback);
                }
            }else{
                logger.info("Queue already contains fact  ["+fact+"]");
            }
        }
    }

    public void solveBlock(Chain chain, Consumer<Block> callback){
        executorService.execute(() -> {
            try{

                var block = new Block();
                if(chain.getChainLength()>0) {
                    block.previousBlockHash = chain.getLastHash();
                }
                synchronized(queue){
                    if(queue.size()<1) return;
                    block.facts.addAll(queue);
                    queue.clear();
                    solving=false;
                }
                block.hash = findHash(block);
                callback.accept(block);

            }catch(Throwable t){
                t.printStackTrace();
            }
        });
    }
    
	public Hash findHash(Block block) {
        block.setNonce( new BigInteger("0") );
        while(true){
            Hash h = Hash.create(block); 
            if(h.asBigInteger().compareTo(target)<0)
                return h;
            block.setNonce(new BigInteger(maximum.bitLength(), new Random()));
        }
    }
    public boolean isEnqueued(Fact fact) {
	    return queue.stream().anyMatch(f -> f.getHash().equals(fact.getHash()));
	}

    public ComplexityDefinition getComplexity()
    { return new ComplexityDefinition(target); }
    public void setComplexity(ComplexityDefinition complexityDefinition)
    { this.target = complexityDefinition.nonceTarget(); }
}
