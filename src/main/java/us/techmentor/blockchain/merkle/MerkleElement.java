package us.techmentor.blockchain.merkle;

import java.util.Optional;

import us.techmentor.Hash;

public class MerkleElement<T> {
    public Optional<MerkleElement<T>> parent = Optional.empty();
    public Optional<MerkleElement<T>> left = Optional.empty();
    public Optional<MerkleElement<T>> right = Optional.empty();
    public Optional<T> value = Optional.empty();
    public Optional<Hash> hash = Optional.empty();
}
