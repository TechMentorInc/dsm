package us.techmentor.blockchain.merkle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import us.techmentor.Hash;

public class MerkleTree<T> {
    MerkleElement<T> root = new MerkleElement<T>();

    public List<T> asList(){
        return crawl(root,new ArrayList<T>());
    }
    private List<T> crawl(MerkleElement<T> element, List<T> list){
        if(element.value.isPresent()) 
            list.add(element.value.get());
        else{
            element.left.map(e -> crawl(e,list));
            element.right.map(e -> crawl(e,list));
        }
        return list;
    }

    public void addAll(Collection<T> collection){
        for (T t : collection) this.add(t);   
    }
    
	public void add(T v) {
        MerkleElement<T> merkleElement = findOpenSlot();
        var e    = new MerkleElement<T>();
        e.value  = Optional.of(v);
        e.parent = Optional.of(merkleElement);
        e.hash   = Optional.of(Hash.getOrCreate(v));

        if(merkleElement!=root){
            merkleElement.left = Optional.of(
                new MerkleElement<T>(){{
                    this.value = merkleElement.value;
                    this.hash = Optional.of(Hash.getOrCreate(this.value.get()));
                    this.parent = Optional.of(merkleElement);
                }}
            );
            merkleElement.value = Optional.empty();
            merkleElement.right = Optional.of(e);
            BranchHash.constructFromLeaf(e);
        }
        else {
            if(merkleElement.left.isEmpty())
                merkleElement.left = Optional.of(e);
            else 
                merkleElement.right = Optional.of(e);    
            BranchHash.constructFromLeaf(e);
        }
	}

    public MerkleElement<T> findOpenSlot(){
        return OpenSlot.find(root,0).element;
    }

	public MerkleElement<T> getRoot() {
		return root;
	}
    public void setRoot(MerkleElement<T> root){
        this.root = root;
    }
	public boolean validate() {
        return verifyElement(root);
	}
    private boolean verifyElement(MerkleElement<T> element){
        if(element.value.isPresent()) {
            if(element.left.isPresent() || element.right.isPresent())
                return false; 
            return true;
        }else{
            var lefthash = element.left.isPresent()?element.left.get().hash.get().getValue():"none";
            var righthash = element.right.isPresent()?element.right.get().hash.get().getValue():"none";
            
            var expectedHash = Hash.create(
                "\""+lefthash+"\":\""+
                    righthash+"\""
            );
            if(!element.hash.get().equals((Object)expectedHash)) return false;
            return  verifyElement(element.left.get()) && 
                    element.right.isPresent()?verifyElement(element.right.get()):true;
        }
    }
}
