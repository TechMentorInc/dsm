package us.techmentor.blockchain.merkle;

import java.util.Optional;

import us.techmentor.Hash;

public class BranchHash {
    public static <T> void constructFromLeaf(MerkleElement<T> leaf){
        leaf.value.map(value -> leaf.hash = Optional.of(Hash.getOrCreate(value)));
        setHashes(leaf.parent.get());
    }
    private static <T> void setHashes(MerkleElement<T> merkleElement){
        
        var leftHash = merkleElement.left.map(e -> e.hash.get().getValue()).orElse("none");
        var rightHash = merkleElement.right.map(e -> e.hash.get().getValue()).orElse("none");
        merkleElement.hash = Optional.of(
            Hash.create(
                "\""+leftHash+"\":\""+
                    rightHash+"\"")
        );

        if(merkleElement.parent.isPresent())
            setHashes(merkleElement.parent.get());
    }
}
