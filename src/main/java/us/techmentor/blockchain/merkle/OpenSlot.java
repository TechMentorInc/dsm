package us.techmentor.blockchain.merkle;

public class OpenSlot {
    public static class MeasuredMerkleElement<T>{
        int elementDepth;
        MerkleElement<T> element;
    }
    public static <T> MeasuredMerkleElement <T> find(MerkleElement <T> start, int depth){
        if(start.left.isEmpty() || start.right.isEmpty()){
            return new MeasuredMerkleElement<T>(){{
                this.elementDepth=depth;
                this.element=start;
            }};
        }else{
            var right = find(start.right.get(),depth+1);
            var left  = find(start.left.get(),depth+1); 
            return right.elementDepth<left.elementDepth ? right : left;
        }
    }
}
