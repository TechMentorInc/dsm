package us.techmentor.blockchain;

import java.util.*;
import com.google.inject.Singleton;
import us.techmentor.blockchain.connector.BlockchainConnector;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.speed.SpeedAdaptingConnector;
import us.techmentor.blockchain.speed.SpeedAdaptingConnectorManager;

@Singleton
public class Blockchain implements SpeedAdaptingConnectorManager {
    Fact result = null;
    Chain chain = new Chain();
    Set<BlockchainConnector> blockchainConnectors =
        new HashSet<BlockchainConnector>();

    public Blockchain(){}

    public Chain getChain() { return chain; }
    public void setChain(Chain chain){ this.chain=chain; }

    public void addRemoteConnectors(List<BlockchainConnector> remoteConnectors) {
        this.blockchainConnectors.addAll(remoteConnectors);
    }
    public void addRemoteConnector(BlockchainConnector remoteConnector) {
        this.blockchainConnectors.add(remoteConnector);
    }

    @Override
    public SpeedAdaptingConnector[] getSpeedAdaptingConnectors() {
        var result = new ArrayList<SpeedAdaptingConnector>();
        var a = blockchainConnectors.toArray();
        for(int i=0; i<a.length; i++){
            var c = a[i];
            if(c instanceof SpeedAdaptingConnector)
                result.add((SpeedAdaptingConnector) c);
        }
        return result.toArray(new SpeedAdaptingConnector[0]);
    }
}
