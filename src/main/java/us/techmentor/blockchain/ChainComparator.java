package us.techmentor.blockchain;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ChainComparator {
    public static class Comparison{
        public int lastCommonIndex;
        public Chain longer;
        public Chain shorter;
        public boolean bothValid;
        public boolean equalLength;
        public String toString(){
            return ToStringBuilder.reflectionToString(this);
        }
    }
    public Comparison compare(Chain b1, Chain b2){
        var result = new Comparison();
        if(b1.getChainLength()>b2.getChainLength()) {
            result.longer=b1;
            result.shorter=b2;
            result.equalLength = false;
        }else if(b1.getChainLength()<b2.getChainLength()){
            result.longer=b2;
            result.shorter=b1;
            result.equalLength = false;
        }else{
            result.equalLength = true;
        }
        findCommonIndex(b1,b2,result);
        checkValidity(b1,b2,result);
        return result;
    }

    private void checkValidity(
        Chain b1, 
        Chain b2, 
        Comparison result){

        result.bothValid = b1.validate() && b2.validate();
    }

    private void findCommonIndex(
        Chain b1, 
        Chain b2, 
        Comparison result){

        b1.commonIndex(b2,result);

    }
}
