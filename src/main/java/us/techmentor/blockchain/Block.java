package us.techmentor.blockchain;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import us.techmentor.Hash;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.merkle.MerkleTree;

public class Block {
    protected Hash hash;

    protected MerkleTree<Fact> facts = new MerkleTree<Fact>();
    protected Hash previousBlockHash;
    protected BigInteger nonce;

	public MerkleTree<Fact> getFacts() {
		return this.facts;
	}
    public void setFacts(MerkleTree<Fact> facts){ this.facts=facts; }

	public Hash getHash() {
		return this.hash;
    }
    public void setHash(Hash hash){
        this.hash=hash;
    }

    public Hash getPreviousBlockHash() {
        return previousBlockHash;
    }
    public void setPreviousBlockHash(Hash previousBlockHash) {
        this.previousBlockHash = previousBlockHash;
    }

    public BigInteger getNonce() {
        return nonce;
    }
    public void setNonce(BigInteger nonce) {
        this.nonce = nonce;
    }

    public List<Fact> factsNotIn(Chain in) {

		var inFacts = in.getAllFacts();
		return facts.asList().stream()
			.filter(f -> !inFacts.contains(f))
			.collect(Collectors.toList());
	}
    
    @Override
    public boolean equals(Object block){
        return this.hash.equals(((Block)block).hash);
    }
    
    public String toString(){
        var p = previousBlockHash==null?null:previousBlockHash.getValue();
        return 
        "[\""+nonce+
        "\",\""+p+
        "\",\""+
            facts.getRoot().hash.map(h->h.getValue())
                .orElse("0")
        +"\"]";
    }

    public boolean validate() {
		return hash.equals(Hash.create(toString()));
	}
}
