package us.techmentor;

public class Numbers {
    public static final byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)((value >> 24) & 0xff),
                (byte)((value >> 16) & 0xff),
                (byte)((value >> 8) & 0xff),
                (byte)(value & 0xff)};
    }
    public static final int byteArrayToInt(byte[] bytes,int start){
        return
        ((bytes[start+0] & 0xFF) << 24) | ((bytes[start+1] & 0xFF) << 16)
                | ((bytes[start+2] & 0xFF) << 8) | (bytes[start+3] & 0xFF);
    }
}
