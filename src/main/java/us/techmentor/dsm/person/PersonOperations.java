package us.techmentor.dsm.person;

import us.techmentor.dsm.metadata.MetaDataOperations;
import us.techmentor.Hash;

import java.security.PublicKey;

import com.google.inject.Inject;

public class PersonOperations {
    @Inject
    MetaDataOperations metaDataOperations;
    public Person createPerson(String name, PublicKey publicKey) {
        try {
            var directory =
                    metaDataOperations.getMetaData().dsmHomeDirectory();

            if(!directory.exists())
                directory.mkdir();

            var person  = new Person();
            person.name = name;
            person.hash = Hash.create(person);
            person.publicKey = publicKey;

            PersonFile.save(directory,person.hash.getValue(),person);
            return person;
        } catch(Exception e) { throw new RuntimeException(e); }
    }

    public Person getPersonByHash(Hash hash) {
        try {
            var directory = metaDataOperations.getMetaData().dsmHomeDirectory();
            return PersonFile.load(directory.toPath(), hash.getValue());
        } catch(Exception e) { throw new RuntimeException(e); }
    }
}
