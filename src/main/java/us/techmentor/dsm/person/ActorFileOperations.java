package us.techmentor.dsm.person;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.Key;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.google.inject.Inject;

import us.techmentor.dsm.metadata.MetaDataOperations;
import us.techmentor.Hash;

public class ActorFileOperations {
    @Inject
    MetaDataOperations metaDataOperations;
    public void savePrivateKey(Hash hash, ActorKey actorKey) throws IOException {
        var directory = getPathAsFile(hash);
        if(!directory.exists())
            directory.mkdir();
        
        var keyfile = Paths.get(directory.toString(),"privatekey");
        FileOutputStream out = null;
        try{
            out=new FileOutputStream(keyfile.toString());
            out.write(actorKey.getPrivateKeyData());
        }finally{
            out.close();
        }
    }
	public Key getPrivateKey(Hash hash, String password) throws IOException {
		var directory = getPathAsFile(hash);
        var keyfile = Paths.get(directory.toString(),"privatekey");
        var in = new FileInputStream(keyfile.toString());
        var keyBytes = new byte[in.available()];
        in.read(keyBytes);
        var actorKey = new ActorKey();
        actorKey.setPassword(password);
        actorKey.setPrivateKey(keyBytes);
        return actorKey.getPrivateKey();
    
	}
    File getPathAsFile(Hash hash){
        var path = Paths.get(
            metaDataOperations.getMetaData().dsmHomeDirectory().toString(),
                hash.getValue());
        if(!path.toFile().exists())
            path.toFile().mkdir();
        return path.toFile();
    }
    public void follow(Hash follower, Hash followed) throws IOException {
        var directory = getPathAsFile(follower);
        var followedfile = Paths.get(directory.toString(),"followed");
        var fos = new FileOutputStream(followedfile.toFile(),true);

        fos.write(("\n"+followed.getValue()).getBytes());
        fos.close();
    }
    public List<Hash> getFolloweredActors(Hash follower) throws IOException {
        var directory = getPathAsFile(follower);
        var followedfile = Paths.get(directory.toString(),"followed");
        if(!followedfile.toFile().exists()) return new ArrayList<Hash>();
        var fis = new FileInputStream(followedfile.toFile());
        var bytes = fis.readAllBytes();
        var result =  Arrays.asList(
            new String(bytes).split("\n")).stream().map(
                (hashvalue) -> {
                    var hashResult = new Hash();
                    hashResult.setValue(hashvalue);
                    return hashResult;

                }
        ).collect(Collectors.toList());
        fis.close();
        return result;
    }
}
