package us.techmentor.dsm.person;

import java.io.IOException;
import java.util.*;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.google.inject.Singleton;
import org.joda.time.DateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.Hash;
import us.techmentor.blockchain.FactOperations;
import us.techmentor.blockchain.fact.Fact;
import us.techmentor.blockchain.fact.FactQuery;
import us.techmentor.blockchain.fact.FactQuery.StringTruth;
import us.techmentor.dsm.Content;

import static us.techmentor.blockchain.PrettyPrint.format;
import static us.techmentor.blockchain.fact.Truths.*;

@Singleton
public class ActorOperations {
    @Inject public ActorFileOperations actorFileOperations;
    @Inject ActorCrypto actorCrypto;
    @Inject FactOperations factOperations;
    Logger logger = LoggerFactory.getLogger(this.getClass());
    Actor currentActor = null;

    public void createActor(String name, String password){
        createActor(name,password,(a)->{});
    }
    public void createActor(
        String name, 
        String password, 
        Consumer<Actor> finished) {
        try {
            logger.info("Creating actor.");
            var actorKey = ActorCrypto.getInstance().createActorKey(password);
            var fact = new Fact(Map.of(
                "type","actor-creation",
                "name",name,
                "public-key",Base64
                            .getEncoder()
                            .encodeToString(
                                actorKey.getPublicKey()
                            )
            ));
            logger.info("Creating Actor with fact: \n"+format(fact));

            var actor = new Actor();
            actor.actorKey = actorKey;
            actor.name = name;
            actor.hash = fact.getHash();

            var createActorFinished = new AtomicInteger();
            factOperations.saveFact(fact,(b)->{
                
                if(createActorFinished.addAndGet(1)==2)
                    finished.accept(actor);
            });
            actorFileOperations.savePrivateKey(
                fact.getHash(), 
                actorKey);
            if(createActorFinished.addAndGet(1)==2)
                finished.accept(actor);

            factOperations.blockUntilFactOnChain(fact);
            
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<Actor> getActorByHash(Hash hash) {
        return factOperations.readFact(new FactQuery(
            Map.of("hash",new StringTruth(hash.getValue()))
        )).map(
            f->{
                return new Actor(){{
                    this.name = f.get("name");
                    this.hash = f.getHash();
                    var actorKey = new ActorKey(){{
                        this.publicKey = 
                        Base64
                            .getDecoder()
                            .decode(
                                f.get("public-key")
                            );
                    }};
                    
                    this.actorKey = actorKey;
                }};
            }
        );
    }
    static Actor factToActor(Fact f){
        return new Actor(){{
            this.name = f.get("name");
            this.hash = f.getHash();
            var actorKey = new ActorKey(){{
                var pk = f.get("public-key");
                this.publicKey = pk!=null?
                Base64
                    .getDecoder()
                    .decode(
                        f.get("public-key")
                    ) : null;
            }};

            this.actorKey = actorKey;
        }};
    }
    public List<Actor> getAllActors(){
        return factOperations.readFacts(
            new FactQuery(
                Map.of("type",new StringTruth("actor-creation"))
            )).stream().map(ActorOperations::factToActor).collect(Collectors.toList());
    }
    public Optional<Actor> getActorByName(String name) {
        return factOperations.readFact(new FactQuery(
                Map.of(
                    "name",new StringTruth(name),
                    "type",new StringTruth("actor-creation"))
        )).map(ActorOperations::factToActor);
    }

	public void become(String name, String password) throws IOException {
        var fact = 
            factOperations.readFact(
                new FactQuery(Map.of(
                    "name",new StringTruth(name),
                    "type",new StringTruth("actor-creation")))
            );
        currentActor = fact.map(ActorOperations::factToActor).get();
        var actorKey = new ActorKey();
        actorKey.setPassword(password);
        actorKey.setPrivateKey(
            actorFileOperations.getPrivateKey(
                currentActor.getHash(), 
                password)
        );
        currentActor.actorKey=actorKey;
        updateFollowing();
    }
    
    public Actor getCurrentActor() throws IOException{
        updateFollowing();
        return this.currentActor;
    }
    public void updateFollowing() throws IOException{
        if (this.currentActor==null) return;
        this.currentActor.following = 
            this.actorFileOperations.getFolloweredActors(this.currentActor.getHash())
            .stream().map(this::getActorByHash)
            .filter(a->a.isPresent())
            .map(a->a.get())
            .collect(Collectors.toList());
    }

    public void post(String content) throws Exception{
        var fact = new Fact(
            Map.of(
                "type","post",
                "content",actorCrypto.privateKeyEncryptBase64(this.currentActor.getActorKey(),content),
                "actor",this.currentActor.getHash().getValue(),
                "when",DateTime.now().toString()
            )
        );
        factOperations.saveFact(fact);
        factOperations.blockUntilFactOnChain(fact);
    }
    public void follow(Actor follower, Actor followed) throws IOException {
        actorFileOperations.follow(
            follower.getHash(), 
            followed.getHash());
        updateFollowing();
    }
    private List<String> following(){
        return currentActor
                .following
                .stream()
                .map(a->a.hash.getValue())
                .collect(Collectors.toList());
    }
    public List<Content> getMostRecentContentByDays(int days) throws Exception{
        var factQuery = new FactQuery(
            Map.of(
                "when",Since(DateTime.now().minusDays(days)),
                "actor",In(following())
            )
        );
        var facts = factOperations.readFacts(factQuery);
        return facts.stream().map(f->{
            try{                
                return new Content(){{
                    this.setActor(
                        currentActor.following
                            .stream()
                            .filter(
                                a->a.getHash().getValue().equals(f.get("actor"))
                            )
                            .findFirst()
                            .get()
                    );

                    this.setContentText(
                        actorCrypto
                            .publicKeyDecryptFromBase64(
                                this.getActor().getActorKey(), 
                                f.get("content"))
                    );
                    this.setHash(Hash.fromString(f.get("hash")));
                    this.setPostTime(DateTime.parse(f.get("when")));
                }};
            } catch(Exception e) {e.printStackTrace();}
            return null;
        }).collect(Collectors.toList());
    }

    public void printTimeline()  {
        List<Content> currentTimeline = new ArrayList<>();
        var done = false;
        while(!done){
            try {
                var mostRecent = getMostRecentContentByDays(10);
                for(int i=0; i < mostRecent.size(); i++) {
                    var article = mostRecent.get(i);
                    if (!currentTimeline.contains(article)) {
                        currentTimeline.add(article);
                        printArticle(article);
                    }
                }
                Thread.sleep(500);
            } catch(Throwable t) {
                System.out.println("Unable to print timeline..");
                throw new RuntimeException(t);
            }
        }
    }
    private static void printArticle(Content article){
        System.out.println("Post by "+article.getActor().getName()+":\n");
        System.out.println("\t>> "+article.getContentText());
        System.out.println("\n\n");
    }
}