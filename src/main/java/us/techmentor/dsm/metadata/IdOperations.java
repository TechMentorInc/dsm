package us.techmentor.dsm.metadata;

import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import us.techmentor.Hash;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.Optional;

public class IdOperations {
    public Optional<String> loadFromDisk(Path dsmPath) {
        var idFile = dsmPath.resolve("id");
        if(!idFile.toFile().exists()) return Optional.empty();
        try {
            var id = Files.readAllLines(idFile).get(0);
            if(id.length() == 0) return Optional.empty();
            return Optional.of(id);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String createId(Path dsmPath) {
        SystemInfo si = new SystemInfo();
        CentralProcessor processor = si.getHardware().getProcessor();
        String serialNumber = processor.getProcessorIdentifier().getProcessorID();
        return Hash.create(serialNumber+"_"+dsmPath.toFile().getAbsolutePath()+"_"+System.currentTimeMillis()).getValue();
    }

    public void save(Path dsmPath, String id) {
        var idPath = dsmPath.resolve("id");
        try {
            if(Files.exists(idPath)) Files.delete(idPath);
            Files.createFile(idPath);
            Files.write(idPath, id.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
