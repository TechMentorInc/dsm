package us.techmentor.dsm.metadata;

import com.google.inject.Inject;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Singleton
public class FileMetaDataOperations {

    @Inject protected IdOperations idOperations;
    private File homeDirectory = SystemUtils.getUserDir();
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private boolean manualSetPossible=true;

    public File getHomeDirectory() { return this.homeDirectory; }
    public void manualSetHomeDirectory(File homeDirectory){
        if(manualSetPossible) this.homeDirectory=homeDirectory;
        else throw new RuntimeException("Manual set unavailable: dsm path already selected");
    }

    public void forceTempHomeDirectory(){
        if(manualSetPossible){
            try {
                this.homeDirectory = Files.createTempDirectory("dsminfotemp").toFile();
                logger.info("Creating Temp Home Directory @ "+this.homeDirectory);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        else throw new RuntimeException("Forcing to temp unavailable: dsm path already selected");
    }

    private Path assembledDirectory(){
        return Paths.get(homeDirectory.getAbsolutePath(),".dsm");
    }

    public Path getDSMPath() {
        this.manualSetPossible = false;
        this.createDSMHome();
        return assembledDirectory();
    }

    public void createDSMHome(){
        this.manualSetPossible = false;
        var directory = assembledDirectory().toFile();
        if(!directory.exists())
            directory.mkdir();
    }

    public synchronized String getID() {
        String id;
        var result = idOperations.loadFromDisk(getDSMPath());

        if(result.isPresent()) id = result.get();
        else {
            id = idOperations.createId(getDSMPath());
            idOperations.save(getDSMPath(),id);
        }
        return id;
    }
}
