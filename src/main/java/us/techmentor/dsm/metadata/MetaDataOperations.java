package us.techmentor.dsm.metadata;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;

@Singleton
public class MetaDataOperations {
    @Inject protected FileMetaDataOperations fileMetaDataOperations;
    public record MetaData(File rootHomeDirectory, File dsmHomeDirectory, String id){}

    public MetaData getMetaData(){
        var id = this.fileMetaDataOperations.getID();
        return new MetaData(
            this.fileMetaDataOperations.getHomeDirectory(),
            this.fileMetaDataOperations.getDSMPath().toFile(),
            id);
    }

    public void manualSetHomeDirectory(File homeDirectory)
    {fileMetaDataOperations.manualSetHomeDirectory(homeDirectory);}

    public void forceTempHomeDirectory()
    {fileMetaDataOperations.forceTempHomeDirectory();}
}
