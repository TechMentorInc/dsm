package us.techmentor.dsm;

import org.joda.time.DateTime;
import us.techmentor.Hash;
import us.techmentor.dsm.person.Actor;

public class Content {
    String contentText;
    Actor actor;
    DateTime postTime;
    Hash hash;
    
    public Actor getActor() {
        return actor;
    }
    public void setActor(Actor actor) {
        this.actor = actor;
    }
    public String getContentText() {
        return this.contentText;
    }
    public void setContentText(String contentText){
        this.contentText = contentText;
    }

    public Hash getHash(){ return this.hash; }
    public void setHash(Hash hash){ this.hash = hash;}

    public DateTime getPostTime(){ return this.postTime; }
    public void setPostTime(DateTime postTime){ this.postTime = postTime; }

    @Override
    public boolean equals(Object o){
        var cmp = ((Content)o).getHash();
        var cur = this.getHash();
        return cmp!=null && cur!=null && cmp.getValue().equals(cur.getValue());
    }

}
