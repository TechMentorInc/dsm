package us.techmentor.dsm;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import us.techmentor.dsm.remotes.RemoteOperations;
import us.techmentor.dsm.person.ActorOperations;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class Heartbeat {
    ScheduledExecutorService stateManagementExecutorService
            = Executors.newSingleThreadScheduledExecutor();
    Logger logger = LoggerFactory.getLogger(this.getClass());

    int connectionTimeout=0;

    @Inject public RemoteOperations remoteOperations;
    @Inject public ActorOperations actorOperations;
    /*
     *  This is basically the heart beat function
     *  - name it better
     *  - add the functionality
     *    > check for dead connections - clear them out
     *    > synch with all remotes (make sure all have the same lead block)
     *  - clean up the "createRemoteConnectors" - at a minimum that should be named better:
     *              "createRemoteConnectorsThatDontAlreadyExist"
     *  - all of this probably needs to be pulled out separately - separate class?
     *    > probably we can start with that refactor - "HeartBeat.java"
     *    >
     */
    public void initiateHeartBeat(int connectionTimeout, int period){
        this.connectionTimeout = connectionTimeout;
        pulse();
        stateManagementExecutorService.scheduleAtFixedRate(this::pulse,0,period,TimeUnit.MILLISECONDS);
    }

    public void pulse(){
        MDC.put("category","pulse");
        logger.info("Pulse...");
        MDC.clear();

        try{
            remoteOperations.createRemoteConnectors(remoteOperations.getAllRemotes());
            remoteOperations.findAndClearDeadConnections(connectionTimeout);
            remoteOperations.synchronizeChain();
            actorOperations.updateFollowing();
        }catch(Exception e){
            MDC.put("category","pulse");
            logger.info("Problem updating following..",e);
            MDC.clear();
        }
    }

    public void stop(){
        this.stateManagementExecutorService.shutdownNow();
    }
}
