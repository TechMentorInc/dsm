package us.techmentor.dsm.connect;

import java.util.ArrayList;
import java.util.List;

import us.techmentor.Hash;

public abstract class Source {
    Hash id;
    List<Subscription> subscriptions = new ArrayList<Subscription>();
    
    public Source(){}
    
    public boolean equals(Object other){
        if(((Source)other).id.equals(this.id)) return true;
        return false;
    }

    public void send(Object data){
        subscriptions.forEach(a->a.call(new SourceTransactionInformation(data)));
    }
}
