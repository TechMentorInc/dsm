package us.techmentor.dsm.connect;

public interface Subscription {
    void call(SourceTransactionInformation sti);
}
