package us.techmentor.dsm.connect;

import java.util.ArrayList;
import java.util.List;

import us.techmentor.Hash;
import us.techmentor.dsm.Remote;

public abstract class SourceCoordination {
    List<Source> sources = new ArrayList<Source>();
    public String addRemoteSource(Remote remote){
        var source = new RemoteSource(remote);
        this.sources.add(source);
        return source.id.getValue();
    }

    public List<Source> getSourcesAsList() {
        return sources;
    }

    public void subscribe(Subscription subscription) {
        getLocalSource().subscriptions.add(subscription);
    }

    public void send(Object data){
        getLocalSource().send(data);
    }

    protected abstract Source getLocalSource();
}

class RemoteSource extends Source{
    Remote remote;
    public RemoteSource(Remote remote){
        this.remote = remote;        
        this.id = Hash.create(this.remote);
    }
    @Override
    public void send(Object data) {}
}
