package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.Blockchain;
import us.techmentor.blockchain.FactOperations;
import us.techmentor.blockchain.connector.BlockchainConnector;
import us.techmentor.blockchain.connector.ClientConnector;
import us.techmentor.blockchain.connector.TCPClientProtocol;
import us.techmentor.dsm.metadata.MetaDataOperations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class RemoteOperations {
    @Inject protected MetaDataOperations metaDataOperations;
    @Inject protected ServerOperations serverOperations;
    @Inject protected Blockchain blockchain;
    @Inject protected FactOperations factOperations;
    @Inject Injector injector;

    Logger logger = LoggerFactory.getLogger(this.getClass());
    List<BlockchainConnector> remoteConnectors = new ArrayList<>();


    public void setFactOperations(FactOperations factOperations) {this.factOperations=factOperations;}
    public void setBlockchain(Blockchain blockchain){
        this.blockchain=blockchain;
    }

    public List<BlockchainConnector> getAllConnectors(){
        List<BlockchainConnector> remotes = new ArrayList<>();
        remotes.addAll(remoteConnectors);
        remotes.addAll(serverOperations.getServerConnectors());
        return remotes;
    }

    public List<String> getAllRemotes(){
        try {
            var path = metaDataOperations.getMetaData().dsmHomeDirectory();
            var remotes = new File(path,"remotes");
            if(!remotes.exists())
                return new ArrayList<>();

            var inputStream = new FileInputStream(remotes);
            var result = Arrays.asList(new String(inputStream.readAllBytes()).split("\n"));
            logger.info("Loading remotes: ["+result+"]");
            inputStream.close();
            return result;
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    public List<String> remoteConnectorsIpAddresses(){
        return remoteConnectors.stream().filter(x->x.getProtocol() instanceof TCPClientProtocol)
                .map(y->(TCPClientProtocol)y.getProtocol())
                .map(z->z.getIpAddress())
                .collect(Collectors.toList());
    }
    public void createRemoteConnectors(List<String> remotes){
        var existingIPs = remoteConnectorsIpAddresses();
        List<BlockchainConnector> tempRemoteConnectors = remotes.stream().filter(x->!existingIPs.contains(x)).map(ipAddress->{
            var protocol = injector.getInstance(TCPClientProtocol.class);
            protocol.setIpAddress(ipAddress);
            var clientConnector = injector.getInstance(ClientConnector.class);
            clientConnector.go(protocol);
            return clientConnector;
        }).collect(Collectors.toList());
        this.remoteConnectors.addAll(tempRemoteConnectors);
        blockchain.addRemoteConnectors(tempRemoteConnectors);

        logger.info("Remote connectors created (w/Registration) count={}", remoteConnectors.size());
    }
    public void createRemoteConnectorsWithoutRegistration(List<String> remotes){
        var existingIPs = remoteConnectorsIpAddresses();
        List<BlockchainConnector> tempRemoteConnectors = remotes.stream().filter(x->!existingIPs.contains(x)).map(ipAddress->{
            var protocol = injector.getInstance(TCPClientProtocol.class);
            protocol.setIpAddress(ipAddress);
            var clientConnector = injector.getInstance(ClientConnector.NoRegistration.class);
            clientConnector.go(protocol);
            return clientConnector;
        }).collect(Collectors.toList());
        this.remoteConnectors.addAll(tempRemoteConnectors);
        blockchain.addRemoteConnectors(tempRemoteConnectors);

        logger.info("Remote connectors created count={}", remoteConnectors.size());
    }
    public void saveRemote(String ip){
        try {
            var firstEntry = false;
            var path = metaDataOperations.getMetaData().dsmHomeDirectory();
            var remotes = new File(path,"remotes");
            if(!remotes.exists()) {
                firstEntry = true;
                remotes.createNewFile();
            }

            var outputStream = new FileOutputStream(remotes,true);
            outputStream.write(((firstEntry?"":"\n")+ip).getBytes());
            outputStream.close();
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    public void findAndClearDeadConnections(int connectionTimeout) {
        serverOperations.timeoutConnections(connectionTimeout);
    }

    public void synchronizeChain() {
        for(var connector : remoteConnectors){
            connector.pullChain();
            connector.updateChain(blockchain.getChain());
        }
    }
}
