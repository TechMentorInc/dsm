package us.techmentor.dsm.remotes;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;


public class NetworkClientAcceptor {
    public record ID(Boolean onlyAddress,byte[] value){}
    public record NetworkClient(String address, InputStream input, OutputStream output, Socket socket, ID id){}
    public static NetworkClient acceptNetworkClient(ServerSocket serverSocket){
        try {
            var socket = serverSocket.accept();
            var inputStream = new BufferedInputStream(socket.getInputStream());
            var id = getID(inputStream,socket);
            return new NetworkClient(
                socket.getInetAddress().getHostAddress(),
                inputStream,
                socket.getOutputStream(),
                socket,id);
        } catch(Throwable t){ throw new RuntimeException(t); }
    }
    public static ID getID(BufferedInputStream inputStream,Socket socket){
        inputStream.mark(1024);
        byte[] in = new byte[2];
        try {
            inputStream.read(in);
            return switch(new String(in)){
                case "ID":
                    in = new byte[8];
                    inputStream.read(in);
                    yield new ID(false,in);
                default:
                    inputStream.reset();
                    yield new ID(true,socket.getInetAddress().getHostAddress().getBytes());
            };
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
