package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.persistence.ChainSerializer;


import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public class ChainReceiverServerAction implements ServerAction {
    @Inject protected ServerOperations serverOperations;
    @Inject protected ChainSerializer chainSerializer;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void serveClient(NetworkClient client) {
        try {
            var serverConnector = serverOperations.getServerConnectorForClient(client);
            var serverProtocol = serverConnector.getProtocol();
            var availableBytes = client.input().available();
            var bytes = new byte[availableBytes];
            client.input().read(bytes);

            var chain = chainSerializer.fromBytes(bytes);
            serverProtocol.addChainToIncomingQueue(chain);
        }catch (Throwable t) { logger.error(t.toString()); }
    }
}
