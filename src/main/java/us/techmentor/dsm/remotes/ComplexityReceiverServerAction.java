package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.techmentor.blockchain.speed.complexity.ComplexityProposal;

public class ComplexityReceiverServerAction implements ServerAction {
    @Inject protected ServerOperations serverOperations;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void serveClient(NetworkClientAcceptor.NetworkClient client) {
        try {
            var serverProtocol = serverOperations.getServerProtocolForClient(client);

            for(int i=0; i<10 && client.input().available()==0; i++){Thread.sleep(100);}
            var availableBytes = client.input().available();
            if(availableBytes==0) {
                logger.info("No bytes...");
                return;
            }
            var bytes = new byte[availableBytes-1];
            client.input().read();
            client.input().read(bytes);

            var rx = new String(bytes);
            logger.info("Received Complexity Bytes: "+rx);

            serverProtocol.addComplexitySuggestionToIncomingQueue(ComplexityProposal.fromString(rx));
        }catch (Throwable t) {throw new RuntimeException(t);}
    }
}
