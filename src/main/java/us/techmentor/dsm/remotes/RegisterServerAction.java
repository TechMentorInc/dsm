package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public class RegisterServerAction implements ServerAction {
    @Inject protected ServerOperations serverOperations;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void serveClient(NetworkClient client) {
        MDC.put("category","network");
        try {
            serverOperations.addClient(client.address());
            client.output().write(("REGISTERED [" + client.address() + "]").getBytes());
        } catch (Throwable t) { logger.error(t.toString());
        } finally { MDC.clear(); }
    }
}
