package us.techmentor.dsm.remotes;

import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public interface ServerAction {
    void serveClient(NetworkClient client);
}

