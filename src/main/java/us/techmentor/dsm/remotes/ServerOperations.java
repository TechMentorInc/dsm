package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

import com.google.inject.Injector;
import com.google.inject.Singleton;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import us.techmentor.blockchain.Blockchain;
import us.techmentor.blockchain.ChainOperations;
import us.techmentor.blockchain.FactOperations;
import us.techmentor.blockchain.connector.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;


@Singleton
public class ServerOperations {
    @Inject Blockchain blockchain;
    @Inject Injector injector;
    @Inject FactOperations factOperations;

    protected Map<String, ServerConnector> clients = new HashMap<>();
    private ServerSocket server;
    boolean mustListen = true;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public void setFactOperations(FactOperations factOperations) {this.factOperations=factOperations;}
    public void setBlockchain(Blockchain blockchain){
        this.blockchain = blockchain;
    }
    public void stopListening(){
        MDC.put("category","network");
        try{
            logger.info("Stop Listening: [currently="+mustListen+"].");
            try {
                this.mustListen=false;
                server.close();
            } catch(Throwable t){  }

            try { Thread.sleep(500); } catch(Throwable t){  }
        }finally{MDC.clear();}
    }

    public void addClient(String ipAddress){
        var protocol = injector.getInstance(TCPServerProtocol.class);
        var connector = injector.getInstance(ServerConnector.class);
        connector.go(protocol);
        clients.put(ipAddress,connector);
        blockchain.addRemoteConnector(connector);
    }

    public ServerConnector getServerConnectorForClient(NetworkClient networkClient){
        return clients.get(networkClient.address());
    }
    public ServerProtocol getServerProtocolForClient(NetworkClient networkClient){
        return getServerConnectorForClient(networkClient).getProtocol();
    }
    public List<ServerConnector> getServerConnectors(){
        return clients.values().stream().toList();
    }

    Map<String,ServerAction> serverActions = null;

    @Inject
    public void setupServerActions(
        RegisterServerAction registerServerAction,
        FactReceiverServerAction factReceiverServerAction,
        ChainReceiverServerAction chainReceiverServerAction,
        GiveFactServerAction giveFactServerAction,
        GiveChainServerAction giveChainServerAction,
        ComplexityReceiverServerAction complexityReceiverServerAction,
        GiveComplexityServerAction giveComplexityServerAction
    ){
        serverActions = Map.of(
            "REGISTER ME",registerServerAction,
            "TAKE FACT",factReceiverServerAction,
            "TAKE CHAIN",chainReceiverServerAction,
            "GIVE FACT",giveFactServerAction,
            "GIVE CHAIN",giveChainServerAction,
            "TAKE COMPLEXITY",complexityReceiverServerAction,
            "GIVE COMPLEXITY",giveComplexityServerAction
        );
    }
    public String getCommand(NetworkClient client)
            throws IOException {
        var bytes = new byte[client.input().read()];
        client.input().read(bytes);
        return new String(bytes);
    }
    public void processCommand(NetworkClient client, String command) {
        MDC.put("category","network");
        try{
            logger.info("Server command: [{}]", command);
            markMostRecentConnection(client);

            serverActions
                    .get(command)
                    .serveClient(client);
            logger.info("Finished Server Command: [{}]", command);
            Thread.sleep(500);

        }catch(Throwable t){
            String stacktrace = ExceptionUtils.getStackTrace(t);
            logger.error("Unable to process server action! {}",stacktrace);

        }finally{MDC.clear();}
    }

    private void markMostRecentConnection(NetworkClient client) {
        var connector = getServerConnectorForClient(client);
        if(connector!=null) connector.setLastCommunicationTimeIsNow();
    }

    public void listen(){
        AtomicBoolean actuallyListening = new AtomicBoolean(false);
        var serverThread = new Runnable() {
            @Override
            public void run() {
                Socket socket = null;
                try {
                    MDC.put("category","network");
                    logger.info("Waiting for connection...(injector={})", injector.hashCode());
                    server = new ServerSocket(6665);
                    actuallyListening.set(true);

                    while(mustListen) {
                        var client = acceptNetworkClient(server);
                        logger.info("Receiving connection...(injector={})", injector.hashCode());

                        var command = getCommand(client);
                        if(valid(client,command))
                            processCommand(client,command);
                    }
                } catch (Throwable t) {
                    String stacktrace = ExceptionUtils.getStackTrace(t);
                    logger.error("Error processing network connection...\n{}", stacktrace);
                } finally {
                    close(socket);
                    MDC.clear();
                }
            }
        };
        var t = new Thread(serverThread);
        t.setDaemon(true);
        t.start();
        waitFor(actuallyListening);
    }

    public void timeoutConnections(int connectionTimeout) {
        MDC.put("category","network");
        try{
            var clients = this.clients.entrySet();
            for(var client : clients){
                var ip = client.getKey();
                var connector = client.getValue();

                int timeSinceLastCommunication = (int)((System.currentTimeMillis()-connector.getLastCommunicationTime()
                )/1000);
                logger.info("Examining IP: {}, tslc={}", ip, timeSinceLastCommunication);

                if(timeSinceLastCommunication > connectionTimeout) {
                    this.clients.remove(ip);
                    logger.info("UNREGISTERED: [{}]", ip);
                }
            }
        }finally{MDC.clear();}
    }
    void close(Socket socket){
        try { socket.close(); }catch(Throwable t){}
    }
    boolean valid(NetworkClient client,String command){
        return command.equals("REGISTER ME") || getServerConnectorForClient(client)!=null;
    }
    void waitFor(AtomicBoolean state){
        while(!state.get()) {
            try { Thread.sleep(500); }
            catch (InterruptedException e) { throw new RuntimeException(e); }
        }
    }
}
