package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;


import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public class GiveFactServerAction implements ServerAction {
    @Inject FactSerializer factSerializer;
    @Inject ServerOperations serverOperations;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void serveClient(NetworkClient client) {
        try {
            MDC.put("category","network");
            logger.info("Sending fact to client [{}].", client.address());
            var bytes =
            serverOperations
                .getServerConnectorForClient(client)
                .getProtocol()
                .getNextFactFromOutgoingQueue()
                .map(f -> factSerializer.asBytes(f));

            if (bytes.isPresent()) {
                logger.info("Sending fact to client [{}]: {}", client.address(), new String(bytes.get()));
                client.output().write(bytes.get());
            }
        } catch (Throwable t){ logger.error(t.toString()); }
          finally{ MDC.clear(); }
    }
}
