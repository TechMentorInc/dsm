package us.techmentor.dsm.remotes;

import com.google.inject.Inject;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static us.techmentor.dsm.remotes.NetworkClientAcceptor.*;

public class FactReceiverServerAction implements ServerAction{
    @Inject
    protected ServerOperations serverOperations;

    @Inject
    protected FactSerializer factSerializer;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void serveClient(NetworkClient client) {
        try {
            var serverConnector = serverOperations.getServerConnectorForClient(client);
            var serverProtocol = serverConnector.getProtocol();

            for(int i=0; i<10 && client.input().available()==0; i++){Thread.sleep(100);}
            var availableBytes = client.input().available();

            if(availableBytes==0) {
                logger.info("No bytes...");
                return;
            }

            var bytes = new byte[availableBytes];
            client.input().read(bytes);
            logger.info("Received Fact Bytes: {}", new String(bytes));
            serverProtocol.addFactToIncomingQueue(factSerializer.asFact(bytes));
        }catch (Throwable t) {
            logger.error("Error received, receiving fact on server: {}", ExceptionUtils.getStackTrace(t));
        }
    }
}
