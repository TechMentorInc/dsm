package us.techmentor.dsm.remotes;

import org.apache.commons.lang3.ArrayUtils;

import us.techmentor.blockchain.fact.Fact;

import java.util.Arrays;
import java.util.HashMap;

public class FactSerializer{
    public byte[] asBytes(Fact fact){
        var map = fact.getValueMap();
        var bytes =
        map.keySet().stream()
            .map(a->("\""+a+"\"=\""+map.get(a)+"\",").getBytes())
            .reduce(
                new byte[0],
                (a,b) -> ArrayUtils.addAll(a,b)
            );
        return Arrays.copyOf(bytes,bytes.length-1);
    }
    public Fact asFact(byte[] bytes){
        var fstring = new String(bytes);
        var fields = fstring.split("(?<!\\\\),");
        var elements = new HashMap<String,String>();
        for (var f: fields) {
            var element = f.split("(?<!\\\\)=");
            elements.put(
                element[0].replaceAll("(?<!\\\\)\"",""),
                element[1].replaceAll("(?<!\\\\)\"","")
            );
        }
        var fact = new Fact(elements);
        return fact;
    }
}
