package us.techmentor.dsm;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.function.Consumer;


import com.google.inject.Inject;
import com.google.inject.Singleton;
import us.techmentor.LogFiltering;
import us.techmentor.blockchain.ChainOperations;
import us.techmentor.blockchain.speed.BlockSpeedAdaptation;
import us.techmentor.dsm.person.Actor;
import us.techmentor.dsm.person.ActorOperations;
import us.techmentor.dsm.remotes.RemoteOperations;
import us.techmentor.dsm.remotes.ServerOperations;


@Singleton
public class DSM {
    @Inject public ServerOperations serverOperations;
    @Inject ActorOperations actorOperations;
    @Inject public RemoteOperations remoteOperations;
    @Inject ChainOperations chainOperations;
    @Inject public BlockSpeedAdaptation blockSpeedAdaptation;
    @Inject public Heartbeat heartbeat;

    public List<Actor> listActors() {
        return actorOperations.getAllActors();
    }

    public void logFilter(String filterCategory) {
        LogFiltering.filterByStringCategory(filterCategory);
    }

    record SimplifiedActor(String name, String public_key, String hash) { }
    boolean listening = false;

    public void createActor(
        String name, 
        String password, 
        Consumer<Actor> finished){
        actorOperations.createActor(
            name, 
            password,
            finished);
    }

    public Actor getActorByName(String name){
        var result = actorOperations.getActorByName(name);
        return result.orElse(null);
    }
    public SimplifiedActor getSimplifiedActorByName(String name){
        var a = getActorByName(name);
        if(a!=null){
            var result = new SimplifiedActor(
            a.getName(),
            Base64
                .getEncoder()
                .encodeToString(a.getActorKey().getPublicKey()),
            a.getHash().getValue());
            return result;
        }
        return null;
    }
    public void stopListening(){
        this.listening=false;
        serverOperations.stopListening();
    }

    public void listen(){
        listening=true;
        serverOperations.listen();
        System.out.println("Waiting for connections...");
        while(listening) {
            try {
                Thread.sleep(1000);
            }catch(Throwable t){ throw new RuntimeException(t); }
        }
    }

    public void become(String name, String password) {
        try{
            actorOperations.become(name,password);
        }catch(IOException ioe){
            throw new RuntimeException(ioe);
        }
    }

    public Actor getCurrentActor(){
        try{
            return actorOperations.getCurrentActor();
        }catch(Exception e){throw new RuntimeException(e);}
    }

    public void post(String content){
        try{
            actorOperations.post(content);
        }catch(Exception e){throw new RuntimeException(e);}
    }
    public void follow(String name){
        follow(getActorByName(name));
    }
	public void follow(Actor toFollow) {
        try{
            actorOperations.follow(this.getCurrentActor(),toFollow);
        }catch(Exception e){throw new RuntimeException(e);}
	}
    public List<Content> getMostRecentContent(int days) {
        try{
            return actorOperations.getMostRecentContentByDays(days);
        }catch(Exception e){throw new RuntimeException(e);}
    }
    public String addRemoteByIPAddress(String ipAddress){
        remoteOperations.saveRemote(ipAddress);
        return "Added remote ["+ipAddress+"]";
    }
    public void regularlyLoadAndSave(){
        chainOperations.initiateRegularLoadAndSave();
    }

    public void beginBlockSpeedAdaptation(){
        blockSpeedAdaptation.start();
    }


    public void init(int connectionTimeout){
        init(connectionTimeout,5000);
    }
    public void init(int connectionTimeout, int heartBeatPeriod){
        blockSpeedAdaptation.start();
        heartbeat.initiateHeartBeat(connectionTimeout,heartBeatPeriod);
        regularlyLoadAndSave();
        beginBlockSpeedAdaptation();
    }

    public void stop(){
        this.serverOperations.stopListening();
        this.blockSpeedAdaptation.stop();
        this.remoteOperations.getAllConnectors().forEach(x->x.stopPolling());
        this.chainOperations.stop();
        this.heartbeat.stop();
    }

    public void monitorTimeline() {
        actorOperations.printTimeline();
    }
}
