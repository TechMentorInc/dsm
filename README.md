<b>DSM - Distributed Social Media</b>
<br>
<br>
It is a commandline tool that allows the user to build a social media network from the ground up without code.
<br><br>
**Install & Run with Homebrew**<br>
(Running dsm with no parameters will give the help screen)
```
brew tap dsm-all/dsm https://gitlab.com/TechMentorInc/dsm-all/homebrew-dsm.git
brew install dsm-cli
dsm
```

**Install & Run with apt**<br><br>
First pull the debian package for the version of dsm that you are interested in - from here: https://gitlab.com/TechMentorInc/dsm-all/dsm/-/packages/.
Then, download it to your Linux machine, and run the following command:
```
sudo apt install ./dsm.deb
dsm
```

**Compiling**
<br><br>
To compile (& test) the project, run the following: 
```
mvn scala:compile
```
**Local Maven Installation** <br><br>
To install the various packages (particularly the debian package) in the local maven repository so that the integration test project can be run against your latest changes, run the following (I like to skip the tests with this one, as I've usually run them as a part of the compile):
```
mvn install -DskipTests=true
```
<hr>

***ADVANCED*** <br><br>
**Setting up DSM Server**<br>
- Start an AWS Ubuntu image on a suitably endowed machine (create a template to save time).
- Download the package that you want from the link above. 
- SCP it to the AWS machine: 
```
scp -i ~/code/key.pem ~/Downloads/dsm-0.0.2-20231130.201535-8.deb ubuntu@<machine-ip-address>:/home/ubuntu/dsm.deb
```
- Start dsm server: 
```
dsm --timeout=20 serve 2>log.file
```



